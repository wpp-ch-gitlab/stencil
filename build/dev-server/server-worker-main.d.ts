import type * as d from '../declarations';
export declare function initServerProcessWorkerProxy(sendToMain: (msg: d.DevServerMessage) => void): (msg: d.DevServerMessage) => void;
