import type * as d from '../../../declarations';
export declare const getCustomElementsBuildConditionals: (config: d.ValidatedConfig, cmps: d.ComponentCompilerMeta[]) => d.BuildConditionals;
