import ts from 'typescript';
import type * as d from '../../../declarations';
export declare const nativeComponentTransform: (compilerCtx: d.CompilerCtx, transformOpts: d.TransformOptions) => ts.TransformerFactory<ts.SourceFile>;
