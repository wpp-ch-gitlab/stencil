import type * as d from '../../declarations';
export declare function proxyHostElement(elm: d.HostElement, cmpMeta: d.ComponentRuntimeMeta): void;
