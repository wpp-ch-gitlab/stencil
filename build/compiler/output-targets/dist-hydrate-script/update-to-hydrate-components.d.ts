import type * as d from '../../../declarations';
export declare const updateToHydrateComponents: (cmps: d.ComponentCompilerMeta[]) => Promise<d.ComponentCompilerData[]>;
