'use strict';

Object.defineProperty(exports, '__esModule', { value: true });

const index = require('./index-cfd579da.js');

/*
 Stencil Client Patch Esm v2.19.0 | MIT Licensed | https://stenciljs.com
 */
const patchEsm = () => {
    return index.promiseResolve();
};

const defineCustomElements = (win, options) => {
  if (typeof window === 'undefined') return Promise.resolve();
  return patchEsm().then(() => {
  return index.bootstrapLazy([["sibling-root.cjs",[[6,"sibling-root","sibling-root"]]]], options);
  });
};

exports.defineCustomElements = defineCustomElements;
