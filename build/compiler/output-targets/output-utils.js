import { flatOne, normalizePath, sortBy } from '@utils';
import { basename, dirname, join, relative } from 'path';
export const relativeImport = (pathFrom, pathTo, ext, addPrefix = true) => {
    let relativePath = relative(dirname(pathFrom), dirname(pathTo));
    if (addPrefix) {
        if (relativePath === '') {
            relativePath = '.';
        }
        else if (relativePath[0] !== '.') {
            relativePath = './' + relativePath;
        }
    }
    return normalizePath(`${relativePath}/${basename(pathTo, ext)}`);
};
export const getComponentsDtsSrcFilePath = (config) => join(config.srcDir, GENERATED_DTS);
export const getComponentsDtsTypesFilePath = (outputTarget) => join(outputTarget.typesDir, GENERATED_DTS);
export const isOutputTargetDist = (o) => o.type === DIST;
export const isOutputTargetDistCollection = (o) => o.type === DIST_COLLECTION;
export const isOutputTargetDistCustomElements = (o) => o.type === DIST_CUSTOM_ELEMENTS;
export const isOutputTargetDistCustomElementsBundle = (o) => o.type === DIST_CUSTOM_ELEMENTS_BUNDLE;
export const isOutputTargetCopy = (o) => o.type === COPY;
export const isOutputTargetDistLazy = (o) => o.type === DIST_LAZY;
export const isOutputTargetAngular = (o) => o.type === ANGULAR;
export const isOutputTargetDistLazyLoader = (o) => o.type === DIST_LAZY_LOADER;
export const isOutputTargetDistGlobalStyles = (o) => o.type === DIST_GLOBAL_STYLES;
export const isOutputTargetHydrate = (o) => o.type === DIST_HYDRATE_SCRIPT;
export const isOutputTargetCustom = (o) => o.type === CUSTOM;
export const isOutputTargetDocs = (o) => o.type === DOCS_README || o.type === DOCS_JSON || o.type === DOCS_CUSTOM || o.type === DOCS_VSCODE;
export const isOutputTargetDocsReadme = (o) => o.type === DOCS_README;
export const isOutputTargetDocsJson = (o) => o.type === DOCS_JSON;
export const isOutputTargetDocsCustom = (o) => o.type === DOCS_CUSTOM;
export const isOutputTargetDocsVscode = (o) => o.type === DOCS_VSCODE;
export const isOutputTargetWww = (o) => o.type === WWW;
export const isOutputTargetStats = (o) => o.type === STATS;
export const isOutputTargetDistTypes = (o) => o.type === DIST_TYPES;
export const getComponentsFromModules = (moduleFiles) => sortBy(flatOne(moduleFiles.map((m) => m.cmps)), (c) => c.tagName);
export const ANGULAR = 'angular';
export const COPY = 'copy';
export const CUSTOM = 'custom';
export const DIST = 'dist';
export const DIST_COLLECTION = 'dist-collection';
export const DIST_CUSTOM_ELEMENTS = 'dist-custom-elements';
export const DIST_CUSTOM_ELEMENTS_BUNDLE = 'dist-custom-elements-bundle';
export const DIST_TYPES = 'dist-types';
export const DIST_HYDRATE_SCRIPT = 'dist-hydrate-script';
export const DIST_LAZY = 'dist-lazy';
export const DIST_LAZY_LOADER = 'dist-lazy-loader';
export const DIST_GLOBAL_STYLES = 'dist-global-styles';
export const DOCS_CUSTOM = 'docs-custom';
export const DOCS_JSON = 'docs-json';
export const DOCS_README = 'docs-readme';
export const DOCS_VSCODE = 'docs-vscode';
export const STATS = 'stats';
export const WWW = 'www';
/**
 * Valid output targets to specify in a Stencil config.
 *
 * Note that there are some output targets (e.g. `DIST_TYPES`) which are
 * programmatically set as output targets by the compiler when other output
 * targets (in that case `DIST`) are set, but which are _not_ supported in a
 * Stencil config. This is enforced in the output target validation code.
 */
export const VALID_CONFIG_OUTPUT_TARGETS = [
    // DIST
    WWW,
    DIST,
    DIST_COLLECTION,
    DIST_CUSTOM_ELEMENTS,
    DIST_CUSTOM_ELEMENTS_BUNDLE,
    DIST_LAZY,
    DIST_HYDRATE_SCRIPT,
    // DOCS
    DOCS_JSON,
    DOCS_README,
    DOCS_VSCODE,
    DOCS_CUSTOM,
    // MISC
    ANGULAR,
    COPY,
    CUSTOM,
    STATS,
];
/**
 * Check whether a given output target is a valid one to be set in a Stencil config
 *
 * @param targetType the type which we want to check
 * @returns whether or not the targetType is a valid, configurable output target.
 */
export function isValidConfigOutputTarget(targetType) {
    // unfortunately `includes` is typed on `ReadonlyArray<T>` as `(el: T):
    // boolean` so a `string` cannot be passed to `includes` on a
    // `ReadonlyArray` 😢 thus we `as any`
    //
    // see microsoft/TypeScript#31018 for some discussion of this
    return VALID_CONFIG_OUTPUT_TARGETS.includes(targetType);
}
export const GENERATED_DTS = 'components.d.ts';
//# sourceMappingURL=output-utils.js.map