import type * as d from '../../../declarations';
export declare const validateHydrateScript: (config: d.ValidatedConfig, userOutputs: d.OutputTarget[]) => d.OutputTargetHydrate[];
