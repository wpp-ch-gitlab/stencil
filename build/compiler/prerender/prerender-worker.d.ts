import type * as d from '../../declarations';
export declare const prerenderWorker: (sys: d.CompilerSystem, prerenderRequest: d.PrerenderUrlRequest) => Promise<d.PrerenderUrlResults>;
