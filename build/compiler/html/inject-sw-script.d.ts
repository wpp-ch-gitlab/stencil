import type * as d from '../../declarations';
export declare const updateIndexHtmlServiceWorker: (config: d.Config, buildCtx: d.BuildCtx, doc: Document, outputTarget: d.OutputTargetWww) => Promise<void>;
