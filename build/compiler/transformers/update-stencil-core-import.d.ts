import ts from 'typescript';
export declare const updateStencilCoreImports: (updatedCoreImportPath: string) => ts.TransformerFactory<ts.SourceFile>;
