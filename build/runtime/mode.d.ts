import type * as d from '../declarations';
export declare const computeMode: (elm: d.HostElement) => string;
export declare const setMode: (handler: d.ResolutionHandler) => number;
export declare const getMode: (ref: d.RuntimeRef) => string;
