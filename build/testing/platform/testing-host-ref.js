import { addHostEventListeners } from '@runtime';
import { hostRefs } from './testing-constants';
export const getHostRef = (elm) => {
    return hostRefs.get(elm);
};
export const registerInstance = (lazyInstance, hostRef) => {
    if (lazyInstance == null || lazyInstance.constructor == null) {
        throw new Error(`Invalid component constructor`);
    }
    if (hostRef == null) {
        const Cstr = lazyInstance.constructor;
        const tagName = Cstr.COMPILER_META && Cstr.COMPILER_META.tagName ? Cstr.COMPILER_META.tagName : 'div';
        const registryTagName = Cstr.COMPILER_META && Cstr.COMPILER_META.registryTagName ? Cstr.COMPILER_META.registryTagName : tagName;
        const elm = document.createElement(registryTagName);
        registerHost(elm, { $flags$: 0, $tagName$: tagName, $registryTagName$: registryTagName });
        hostRef = getHostRef(elm);
    }
    hostRef.$lazyInstance$ = lazyInstance;
    return hostRefs.set(lazyInstance, hostRef);
};
export const registerHost = (elm, cmpMeta) => {
    const hostRef = {
        $flags$: 0,
        $hostElement$: elm,
        $cmpMeta$: cmpMeta,
        $instanceValues$: new Map(),
        $renderCount$: 0,
    };
    hostRef.$onInstancePromise$ = new Promise((r) => (hostRef.$onInstanceResolve$ = r));
    hostRef.$onReadyPromise$ = new Promise((r) => (hostRef.$onReadyResolve$ = r));
    elm['s-p'] = [];
    elm['s-rc'] = [];
    addHostEventListeners(elm, hostRef, cmpMeta.$listeners$, false);
    hostRefs.set(elm, hostRef);
};
//# sourceMappingURL=testing-host-ref.js.map