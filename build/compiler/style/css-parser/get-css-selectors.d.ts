export declare const getCssSelectors: (sel: string) => {
    all: string[];
    tags: string[];
    classNames: string[];
    ids: string[];
    attrs: string[];
};
