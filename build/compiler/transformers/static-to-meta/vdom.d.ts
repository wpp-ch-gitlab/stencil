import ts from 'typescript';
import type * as d from '../../../declarations';
export declare const gatherVdomMeta: (m: d.Module | d.ComponentCompilerMeta, args: ts.NodeArray<ts.Expression>) => void;
