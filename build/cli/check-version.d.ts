import type { ValidatedConfig } from '../declarations';
export declare const startCheckVersion: (config: ValidatedConfig, currentVersion: string) => Promise<() => void>;
export declare const printCheckVersionResults: (versionChecker: Promise<() => void>) => Promise<void>;
