import type * as d from '../../declarations';
export declare const validateDevServer: (config: d.ValidatedConfig, diagnostics: d.Diagnostic[]) => d.DevServerConfig | undefined;
