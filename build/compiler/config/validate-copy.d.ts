import type * as d from '../../declarations';
export declare const validateCopy: (copy: d.CopyTask[] | boolean, defaultCopy?: d.CopyTask[]) => d.CopyTask[];
