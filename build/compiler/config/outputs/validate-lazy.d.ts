import type * as d from '../../../declarations';
export declare const validateLazy: (config: d.ValidatedConfig, userOutputs: d.OutputTarget[]) => d.OutputTargetDistLazy[];
