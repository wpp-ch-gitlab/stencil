/// <reference types="node" />
/// <reference types="node" />
/// <reference types="node" />
/// <reference types="node" />
/// <reference types="node" />
import { TerminalLoggerSys } from '../../compiler/sys/logger/terminal-logger';
import { Logger } from '../../declarations';
/**
 * Create a logger to run in a Node environment
 * @param context a context with NodeJS specific details used to create the logger
 * @returns the created logger
 */
export declare const createNodeLogger: (context: {
    process: NodeJS.Process;
}) => Logger;
/**
 * Create a logger sys object for use in a Node.js environment
 *
 * The `TerminalLoggerSys` interface basically abstracts away some
 * environment-specific details so that the terminal logger can deal with
 * things in a (potentially) platform-agnostic way.
 *
 * @param prcs the current node.js process object
 * @returns a configured logger sys object
 */
export declare function createNodeLoggerSys(prcs: NodeJS.Process): TerminalLoggerSys;
