/**
 * Virtual DOM patching algorithm based on Snabbdom by
 * Simon Friis Vindum (@paldepind)
 * Licensed under the MIT License
 * https://github.com/snabbdom/snabbdom/blob/master/LICENSE
 *
 * Modified for Stencil's renderer and slot projection
 */
let e, t, n, o = !1, l = !1, s = !1, r = !1;

const isComplexType = e => "object" === (
// https://jsperf.com/typeof-fn-object/5
e = typeof e) || "function" === e, h = (e, t, ...n) => {
  let o = null, l = null, s = !1, r = !1;
  const c = [], walk = t => {
    for (let n = 0; n < t.length; n++) o = t[n], Array.isArray(o) ? walk(o) : null != o && "boolean" != typeof o && ((s = "function" != typeof e && !isComplexType(o)) && (o += ""), 
    s && r ? 
    // If the previous child was simple (string), we merge both
    c[c.length - 1]._$$text$$_ += o : 
    // Append a new vNode, if it's text, we create a text vNode
    c.push(s ? newVNode(null, o) : o), r = s);
  };
  walk(n), t && t.name && (l = t.name);
  const i = newVNode(e, null);
  return i._$$attrs$$_ = t, c.length > 0 && (i._$$children$$_ = c), i._$$name$$_ = l, 
  i;
}, newVNode = (e, t) => {
  const n = {
    _$$flags$$_: 0,
    _$$tag$$_: e,
    _$$text$$_: t,
    _$$elm$$_: null,
    _$$children$$_: null,
    _$$name$$_: null
  };
  return n;
}, c = {}, emitEvent = (e, t, n) => {
  const o = p.ce(t, n);
  return e.dispatchEvent(o), o;
}, i =  new WeakMap, attachStyles = e => {
  const t = e._$$cmpMeta$$_, n = e._$$hostElement$$_, o = t._$$flags$$_, l = (t._$$tagName$$_, 
  () => {}), s = ((e, t, n, o) => {
    let l = getScopeId(t);
    const s = $.get(l);
    // if an element is NOT connected then getRootNode() will return the wrong root node
    // so the fallback is to always use the document for the root node in those cases
        if (e = 11 /* NODE_TYPE.DocumentFragment */ === e.nodeType ? e : m, s) if ("string" == typeof s) {
      e = e.head || e;
      let t, n = i.get(e);
      n || i.set(e, n = new Set), n.has(l) || (t = m.createElement("style"), t.innerHTML = s, 
      e.insertBefore(t, e.querySelector("link")), n && n.add(l));
    } else e.adoptedStyleSheets.includes(s) || (e.adoptedStyleSheets = [ ...e.adoptedStyleSheets, s ]);
    return l;
  })(n.getRootNode(), t);
  10 /* CMP_FLAGS.needsScopedEncapsulation */ & o && (
  // only required when we're NOT using native shadow dom (slot)
  // or this browser doesn't support native shadow dom
  // and this host element was NOT created with SSR
  // let's pick out the inner content for slot projection
  // create a node to represent where the original
  // content was first placed, which is useful later on
  // DOM WRITE!!
  n["s-sc"] = s, n.classList.add(s + "-h"), 2 /* CMP_FLAGS.scopedCssEncapsulation */ & o && n.classList.add(s + "-s")), 
  l();
}, getScopeId = (e, t) => "sc-" + e._$$tagName$$_
/**
 * Create a DOM Node corresponding to one of the children of a given VNode.
 *
 * @param oldParentVNode the parent VNode from the previous render
 * @param newParentVNode the parent VNode from the current render
 * @param childIndex the index of the VNode, in the _new_ parent node's
 * children, for which we will create a new DOM node
 * @param parentElm the parent DOM node which our new node will be a child of
 * @returns the newly created node
 */ , createElm = (l, r, c, i) => {
  // tslint:disable-next-line: prefer-const
  const a = r._$$children$$_[c];
  let f, u, $, d = 0;
  if (o || (
  // remember for later we need to check to relocate nodes
  s = !0, "slot" === a._$$tag$$_ && (e && 
  // scoped css needs to add its scoped id to the parent element
  i.classList.add(e + "-s"), a._$$flags$$_ |= a._$$children$$_ ? // slot element has fallback content
  2 /* VNODE_FLAGS.isSlotFallback */ : // slot element does not have fallback content
  1 /* VNODE_FLAGS.isSlotReference */)), null !== a._$$text$$_) 
  // create text node
  f = a._$$elm$$_ = m.createTextNode(a._$$text$$_); else if (1 /* VNODE_FLAGS.isSlotReference */ & a._$$flags$$_) 
  // create a slot reference node
  f = a._$$elm$$_ = slotReferenceDebugNode(a); else if (
  // create element
  f = a._$$elm$$_ = m.createElement(2 /* VNODE_FLAGS.isSlotFallback */ & a._$$flags$$_ ? "slot-fb" : a._$$tag$$_), 
  (e => null != e)(e) && f["s-si"] !== e && 
  // if there is a scopeId and this is the initial render
  // then let's add the scopeId as a css class
  f.classList.add(f["s-si"] = e), a._$$children$$_) for (d = 0; d < a._$$children$$_.length; ++d) 
  // create the node
  u = createElm(l, a, d, f), 
  // return node could have been null
  u && 
  // append our new node
  f.appendChild(u);
  return f["s-hn"] = n, 3 /* VNODE_FLAGS.isSlotReference */ & a._$$flags$$_ && (
  // remember the content reference comment
  f["s-sr"] = !0, 
  // remember the content reference comment
  f["s-cr"] = t, 
  // remember the slot name, or empty string for default slot
  f["s-sn"] = a._$$name$$_ || "", 
  // check if we've got an old vnode for this slot
  $ = l && l._$$children$$_ && l._$$children$$_[c], $ && $._$$tag$$_ === a._$$tag$$_ && l._$$elm$$_ && 
  // we've got an old slot vnode and the wrapper is being replaced
  // so let's move the old slot content back to it's original location
  putBackInOriginalLocation(l._$$elm$$_, !1)), f;
}, putBackInOriginalLocation = (e, t) => {
  p._$$flags$$_ |= 1 /* PLATFORM_FLAGS.isTmpDisconnected */;
  const o = e.childNodes;
  for (let e = o.length - 1; e >= 0; e--) {
    const l = o[e];
    l["s-hn"] !== n && l["s-ol"] && (
    // // this child node in the old element is from another component
    // // remove this node from the old slot's parent
    // childNode.remove();
    // and relocate it back to it's original location
    parentReferenceNode(l).insertBefore(l, referenceNode(l)), 
    // remove the old original location comment entirely
    // later on the patch function will know what to do
    // and move this to the correct spot in need be
    l["s-ol"].remove(), l["s-ol"] = void 0, s = !0), t && putBackInOriginalLocation(l, t);
  }
  p._$$flags$$_ &= -2 /* PLATFORM_FLAGS.isTmpDisconnected */;
}, referenceNode = e => e && e["s-ol"] || e, parentReferenceNode = e => (e["s-ol"] ? e["s-ol"] : e).parentNode
/**
 * Handle reconciling an outdated VNode with a new one which corresponds to
 * it. This function handles flushing updates to the DOM and reconciling the
 * children of the two nodes (if any).
 *
 * @param oldVNode an old VNode whose DOM element and children we want to update
 * @param newVNode a new VNode representing an updated version of the old one
 */ , patch = (e, t) => {
  const n = t._$$elm$$_ = e._$$elm$$_, o = t._$$children$$_, l = t._$$text$$_;
  let s;
  null === l ? null !== o && 
  // add the new vnode children
  ((e, t, n, o, l, s) => {
    let r, c = e["s-cr"] && e["s-cr"].parentNode || e;
    for (;l <= s; ++l) o[l] && (r = createElm(null, n, l, e), r && (o[l]._$$elm$$_ = r, 
    c.insertBefore(r, referenceNode(t))));
  })(n, null, t, o, 0, o.length - 1) : (s = n["s-cr"]) ? 
  // this element has slotted content
  s.parentNode.textContent = l : e._$$text$$_ !== l && (
  // update the text content for the text only vnode
  // and also only if the text is different than before
  n.data = l);
}, updateFallbackSlotVisibility = e => {
  // tslint:disable-next-line: prefer-const
  const t = e.childNodes;
  let n, o, l, s, r, c;
  for (o = 0, l = t.length; o < l; o++) if (n = t[o], 1 /* NODE_TYPE.ElementNode */ === n.nodeType) {
    if (n["s-sr"]) for (
    // this is a slot fallback node
    // get the slot name for this slot reference node
    r = n["s-sn"], 
    // by default always show a fallback slot node
    // then hide it if there are other slots in the light dom
    n.hidden = !1, s = 0; s < l; s++) if (c = t[s].nodeType, t[s]["s-hn"] !== n["s-hn"] || "" !== r) {
      // this sibling node is from a different component OR is a named fallback slot node
      if (1 /* NODE_TYPE.ElementNode */ === c && r === t[s].getAttribute("slot")) {
        n.hidden = !0;
        break;
      }
    } else 
    // this is a default fallback slot node
    // any element or text node (with content)
    // should hide the default fallback slot node
    if (1 /* NODE_TYPE.ElementNode */ === c || 3 /* NODE_TYPE.TextNode */ === c && "" !== t[s].textContent.trim()) {
      n.hidden = !0;
      break;
    }
    // keep drilling down
        updateFallbackSlotVisibility(n);
  }
}, a = [], relocateSlotContent = e => {
  // tslint:disable-next-line: prefer-const
  let t, n, o, s, r, c, i = 0;
  const f = e.childNodes, u = f.length;
  for (;i < u; i++) {
    if (t = f[i], t["s-sr"] && (n = t["s-cr"]) && n.parentNode) for (
    // first got the content reference comment node
    // then we got it's parent, which is where all the host content is in now
    o = n.parentNode.childNodes, s = t["s-sn"], c = o.length - 1; c >= 0; c--) n = o[c], 
    n["s-cn"] || n["s-nr"] || n["s-hn"] === t["s-hn"] || (
    // let's do some relocating to its new home
    // but never relocate a content reference node
    // that is suppose to always represent the original content location
    isNodeLocatedInSlot(n, s) ? (
    // it's possible we've already decided to relocate this node
    r = a.find((e => e._$$nodeToRelocate$$_ === n)), 
    // made some changes to slots
    // let's make sure we also double check
    // fallbacks are correctly hidden or shown
    l = !0, n["s-sn"] = n["s-sn"] || s, r ? 
    // previously we never found a slot home for this node
    // but turns out we did, so let's remember it now
    r._$$slotRefNode$$_ = t : 
    // add to our list of nodes to relocate
    a.push({
      _$$slotRefNode$$_: t,
      _$$nodeToRelocate$$_: n
    }), n["s-sr"] && a.map((e => {
      isNodeLocatedInSlot(e._$$nodeToRelocate$$_, n["s-sn"]) && (r = a.find((e => e._$$nodeToRelocate$$_ === n)), 
      r && !e._$$slotRefNode$$_ && (e._$$slotRefNode$$_ = r._$$slotRefNode$$_));
    }))) : a.some((e => e._$$nodeToRelocate$$_ === n)) || 
    // so far this element does not have a slot home, not setting slotRefNode on purpose
    // if we never find a home for this element then we'll need to hide it
    a.push({
      _$$nodeToRelocate$$_: n
    }));
    1 /* NODE_TYPE.ElementNode */ === t.nodeType && relocateSlotContent(t);
  }
}, isNodeLocatedInSlot = (e, t) => 1 /* NODE_TYPE.ElementNode */ === e.nodeType ? null === e.getAttribute("slot") && "" === t || e.getAttribute("slot") === t : e["s-sn"] === t || "" === t, renderVdom = (r, i) => {
  const f = r._$$hostElement$$_, u = r._$$cmpMeta$$_, $ = r._$$vnode$$_ || newVNode(null, null), d = (e => e && e._$$tag$$_ === c)
  /**
 * Helper function to create & dispatch a custom Event on a provided target
 * @param elm the target of the Event
 * @param name the name to give the custom Event
 * @param opts options for configuring a custom Event
 * @returns the custom Event
 */ (i) ? i : h(null, null, i);
  if (n = f.tagName, d._$$tag$$_ = null, d._$$flags$$_ |= 4 /* VNODE_FLAGS.isHost */ , 
  r._$$vnode$$_ = d, d._$$elm$$_ = $._$$elm$$_ = f, e = f["s-sc"], t = f["s-cr"], 
  o = 0 != (1 /* CMP_FLAGS.shadowDomEncapsulation */ & u._$$flags$$_), 
  // always reset
  l = !1, 
  // synchronous patch
  patch($, d), 
  // while we're moving nodes around existing nodes, temporarily disable
  // the disconnectCallback from working
  p._$$flags$$_ |= 1 /* PLATFORM_FLAGS.isTmpDisconnected */ , s) {
    let e, t, n, o, l, s;
    relocateSlotContent(d._$$elm$$_);
    let r = 0;
    for (;r < a.length; r++) e = a[r], t = e._$$nodeToRelocate$$_, t["s-ol"] || (
    // add a reference node marking this node's original location
    // keep a reference to this node for later lookups
    n = originalLocationDebugNode(t), n["s-nr"] = t, t.parentNode.insertBefore(t["s-ol"] = n, t));
    for (r = 0; r < a.length; r++) if (e = a[r], t = e._$$nodeToRelocate$$_, e._$$slotRefNode$$_) {
      for (
      // by default we're just going to insert it directly
      // after the slot reference node
      o = e._$$slotRefNode$$_.parentNode, l = e._$$slotRefNode$$_.nextSibling, n = t["s-ol"]; n = n.previousSibling; ) if (s = n["s-nr"], 
      s && s["s-sn"] === t["s-sn"] && o === s.parentNode && (s = s.nextSibling, !s || !s["s-nr"])) {
        l = s;
        break;
      }
      (!l && o !== t.parentNode || t.nextSibling !== l) && t !== l && (!t["s-hn"] && t["s-ol"] && (
      // probably a component in the index.html that doesn't have it's hostname set
      t["s-hn"] = t["s-ol"].parentNode.nodeName), 
      // add it back to the dom but in its new home
      o.insertBefore(t, l));
    } else 
    // this node doesn't have a slot home to go to, so let's hide it
    1 /* NODE_TYPE.ElementNode */ === t.nodeType && (t.hidden = !0);
  }
  l && updateFallbackSlotVisibility(d._$$elm$$_), 
  // done moving nodes around
  // allow the disconnect callback to work again
  p._$$flags$$_ &= -2 /* PLATFORM_FLAGS.isTmpDisconnected */ , 
  // always reset
  a.length = 0;
}, slotReferenceDebugNode = e => m.createComment(`<slot${e._$$name$$_ ? ' name="' + e._$$name$$_ + '"' : ""}> (host=${n.toLowerCase()})`), originalLocationDebugNode = e => m.createComment("org-location for " + (e.localName ? `<${e.localName}> (host=${e["s-hn"]})` : `[${e.textContent}]`)), attachToAncestor = (e, t) => {
  t && !e._$$onRenderResolve$$_ && t["s-p"] && t["s-p"].push(new Promise((t => e._$$onRenderResolve$$_ = t)));
}, scheduleUpdate = (e, t) => {
  if (4 /* HOST_FLAGS.isWaitingForChildren */ & e._$$flags$$_) return void (e._$$flags$$_ |= 512 /* HOST_FLAGS.needsRerender */);
  attachToAncestor(e, e._$$ancestorComponent$$_);
  return g((() => dispatchHooks(e, t)));
}, dispatchHooks = (e, t) => {
  const n = e._$$hostElement$$_, o = (e._$$cmpMeta$$_._$$tagName$$_, () => {}), l = e._$$lazyInstance$$_;
  let s;
  return t ? (emitLifecycleEvent(n, "componentWillLoad"), s = safeCall(l, "componentWillLoad")) : emitLifecycleEvent(n, "componentWillUpdate"), 
  emitLifecycleEvent(n, "componentWillRender"), o(), then(s, (() => updateComponent(e, l, t)));
}, updateComponent = async (e, t, n) => {
  // updateComponent
  const o = e._$$hostElement$$_, l = (e._$$cmpMeta$$_._$$tagName$$_, () => {}), s = o["s-rc"];
  n && 
  // DOM WRITE!
  attachStyles(e);
  const r = (e._$$cmpMeta$$_._$$tagName$$_, () => {});
  callRender(e, t), s && (
  // ok, so turns out there are some child host elements
  // waiting on this parent element to load
  // let's fire off all update callbacks waiting
  s.map((e => e())), o["s-rc"] = void 0), r(), l();
  {
    const t = o["s-p"], postUpdate = () => postUpdateComponent(e);
    0 === t.length ? postUpdate() : (Promise.all(t).then(postUpdate), e._$$flags$$_ |= 4 /* HOST_FLAGS.isWaitingForChildren */ , 
    t.length = 0);
  }
}, callRender = (e, t, n) => {
  try {
    t = t.render(), e._$$flags$$_ |= 2 /* HOST_FLAGS.hasRendered */ , renderVdom(e, t);
  } catch (t) {
    consoleError(t, e._$$hostElement$$_);
  }
  return null;
}, postUpdateComponent = e => {
  e._$$cmpMeta$$_._$$tagName$$_;
  const t = e._$$hostElement$$_, endPostUpdate = () => {}, n = e._$$ancestorComponent$$_;
  emitLifecycleEvent(t, "componentDidRender"), 64 /* HOST_FLAGS.hasLoadedComponent */ & e._$$flags$$_ ? (emitLifecycleEvent(t, "componentDidUpdate"), 
  endPostUpdate()) : (e._$$flags$$_ |= 64 /* HOST_FLAGS.hasLoadedComponent */ , 
  // DOM WRITE!
  addHydratedFlag(t), emitLifecycleEvent(t, "componentDidLoad"), endPostUpdate(), 
  e._$$onReadyResolve$$_(t), n || appDidLoad()), e._$$onRenderResolve$$_ && (e._$$onRenderResolve$$_(), 
  e._$$onRenderResolve$$_ = void 0), 512 /* HOST_FLAGS.needsRerender */ & e._$$flags$$_ && nextTick((() => scheduleUpdate(e, !1))), 
  e._$$flags$$_ &= -517 /* HOST_FLAGS.needsRerender */;
}
// ( •_•)
// ( •_•)>⌐■-■
// (⌐■_■)
, appDidLoad = e => {
  addHydratedFlag(m.documentElement), nextTick((() => emitEvent(d, "appload", {
    detail: {
      namespace: "testsibling"
    }
  })));
}, safeCall = (e, t, n) => {
  if (e && e[t]) try {
    return e[t](n);
  } catch (e) {
    consoleError(e);
  }
}, then = (e, t) => e && e.then ? e.then(t) : t(), emitLifecycleEvent = (e, t) => {
  emitEvent(e, "stencil_" + t, {
    bubbles: !0,
    composed: !0,
    detail: {
      namespace: "testsibling"
    }
  });
}, addHydratedFlag = e => e.classList.add("hydrated")
/**
 * Attach a series of runtime constructs to a compiled Stencil component
 * constructor, including getters and setters for the `@Prop` and `@State`
 * decorators, callbacks for when attributes change, and so on.
 *
 * @param Cstr the constructor for a component that we need to process
 * @param cmpMeta metadata collected previously about the component
 * @param flags a number used to store a series of bit flags
 * @returns a reference to the same constructor passed in (but now mutated)
 */ , proxyComponent = (e, t, n) => {
  if (t._$$members$$_) {
    Object.entries(t._$$members$$_).map((([e, [t]]) => {}));
  }
  return e;
}, initializeComponent = async (e, t, n, o, l) => {
  // initializeComponent
  if (0 == (32 /* HOST_FLAGS.hasInitializedComponent */ & t._$$flags$$_)) {
    {
      if (
      // we haven't initialized this element yet
      t._$$flags$$_ |= 32 /* HOST_FLAGS.hasInitializedComponent */ , (
      // lazy loaded components
      // request the component's implementation to be
      // wired up with the host element
      l = loadModule(n)).then) {
        // Await creates a micro-task avoid if possible
        const endLoad = () => {};
        l = await l, endLoad();
      }
      if (!l) throw Error(`Constructor for "${n._$$tagName$$_}#${t._$$modeName$$_}" was not found`);
      l.isProxied || (proxyComponent(l, n), l.isProxied = !0);
      const e = (n._$$tagName$$_, () => {});
      // ok, time to construct the instance
      // but let's keep track of when we start and stop
      // so that the getters/setters don't incorrectly step on data
            t._$$flags$$_ |= 8 /* HOST_FLAGS.isConstructingInstance */;
      // construct the lazy-loaded component implementation
      // passing the hostRef is very important during
      // construction in order to directly wire together the
      // host element and the lazy-loaded instance
      try {
        new l(t);
      } catch (e) {
        consoleError(e);
      }
      t._$$flags$$_ &= -9 /* HOST_FLAGS.isConstructingInstance */ , e();
    }
    if (l.style) {
      // this component has styles but we haven't registered them yet
      let e = l.style;
      const t = getScopeId(n);
      if (!$.has(t)) {
        const o = (n._$$tagName$$_, () => {});
        ((e, t, n) => {
          let o = $.get(e);
          y && n ? (o = o || new CSSStyleSheet, "string" == typeof o ? o = t : o.replaceSync(t)) : o = t, 
          $.set(e, o);
        })(t, e, !!(1 /* CMP_FLAGS.shadowDomEncapsulation */ & n._$$flags$$_)), o();
      }
    }
  }
  // we've successfully created a lazy instance
    const s = t._$$ancestorComponent$$_, schedule = () => scheduleUpdate(t, !0);
  s && s["s-rc"] ? 
  // this is the initial load and this component it has an ancestor component
  // but the ancestor component has NOT fired its will update lifecycle yet
  // so let's just cool our jets and wait for the ancestor to continue first
  // this will get fired off when the ancestor component
  // finally gets around to rendering its lazy self
  // fire off the initial update
  s["s-rc"].push(schedule) : schedule();
}, setContentReference = e => {
  // only required when we're NOT using native shadow dom (slot)
  // or this browser doesn't support native shadow dom
  // and this host element was NOT created with SSR
  // let's pick out the inner content for slot projection
  // create a node to represent where the original
  // content was first placed, which is useful later on
  const t = e["s-cr"] = m.createComment(`content-ref (host=${e.localName})`);
  t["s-cn"] = !0, e.insertBefore(t, e.firstChild);
}, bootstrapLazy = (e, t = {}) => {
  const endBootstrap = () => {}, n = [], o = t.exclude || [], l = d.customElements, s = m.head, r =  s.querySelector("meta[charset]"), c =  m.createElement("style"), i = [];
  let a, f = !0;
  Object.assign(p, t), p._$$resourcesUrl$$_ = new URL(t.resourcesUrl || "./", m.baseURI).href, 
  e.map((e => {
    e[1].map((t => {
      const s = {
        _$$flags$$_: t[0],
        _$$tagName$$_: t[1],
        _$$registryTagName$$_: t[2],
        _$$members$$_: t[3],
        _$$listeners$$_: t[4]
      };
      s._$$members$$_ = t[3];
      const r = s._$$tagName$$_, c = class extends HTMLElement {
        // StencilLazyHost
        constructor(e) {
          // @ts-ignore
          super(e), registerHost(e = this, s);
        }
        connectedCallback() {
          a && (clearTimeout(a), a = null), f ? 
          // connectedCallback will be processed once all components have been registered
          i.push(this) : p.jmp((() => (e => {
            if (0 == (1 /* PLATFORM_FLAGS.isTmpDisconnected */ & p._$$flags$$_)) {
              const t = getHostRef(e), n = t._$$cmpMeta$$_, o = (n._$$tagName$$_, () => {});
              if (!(1 /* HOST_FLAGS.hasConnected */ & t._$$flags$$_)) {
                // first time this component has connected
                t._$$flags$$_ |= 1 /* HOST_FLAGS.hasConnected */ , 
                // initUpdate
                // if the slot polyfill is required we'll need to put some nodes
                // in here to act as original content anchors as we move nodes around
                // host element has been connected to the DOM
                12 /* CMP_FLAGS.needsShadowDomShim */ & n._$$flags$$_ && setContentReference(e);
                {
                  // find the first ancestor component (if there is one) and register
                  // this component as one of the actively loading child components for its ancestor
                  let n = e;
                  for (;n = n.parentNode || n.host; ) 
                  // climb up the ancestors looking for the first
                  // component that hasn't finished its lifecycle update yet
                  if (n["s-p"]) {
                    // we found this components first ancestor component
                    // keep a reference to this component's ancestor component
                    attachToAncestor(t, t._$$ancestorComponent$$_ = n);
                    break;
                  }
                }
                initializeComponent(0, t, n);
              }
              o();
            }
          })(this)));
        }
        disconnectedCallback() {
          p.jmp((() => (e => {
            0 == (1 /* PLATFORM_FLAGS.isTmpDisconnected */ & p._$$flags$$_) && getHostRef(e);
          })(this)));
        }
        componentOnReady() {
          return getHostRef(this)._$$onReadyPromise$$_;
        }
      };
      s._$$lazyBundleId$$_ = e[0], o.includes(r) || l.get(r) || (n.push(r), l.define(r, proxyComponent(c, s)));
    }));
  })), c.innerHTML = n + "{visibility:hidden}.hydrated{visibility:inherit}", c.setAttribute("data-styles", ""), 
  s.insertBefore(c, r ? r.nextSibling : s.firstChild), 
  // Process deferred connectedCallbacks now all components have been registered
  f = !1, i.length ? i.map((e => e.connectedCallback())) : p.jmp((() => a = setTimeout(appDidLoad, 30))), 
  // Fallback appLoad event
  endBootstrap();
}, f =  new WeakMap, getHostRef = e => f.get(e), registerInstance = (e, t) => f.set(t._$$lazyInstance$$_ = e, t), registerHost = (e, t) => {
  const n = {
    _$$flags$$_: 0,
    _$$hostElement$$_: e,
    _$$cmpMeta$$_: t,
    _$$instanceValues$$_: new Map
  };
  return n._$$onReadyPromise$$_ = new Promise((e => n._$$onReadyResolve$$_ = e)), 
  e["s-p"] = [], e["s-rc"] = [], f.set(e, n);
}, consoleError = (e, t) => (0, console.error)(e, t), u =  new Map, loadModule = (e, t, n) => {
  // loadModuleImport
  const o = e._$$tagName$$_.replace(/-/g, "_"), l = e._$$lazyBundleId$$_, s = u.get(l);
  return s ? s[o] : import(
  /* @vite-ignore */
  /* webpackInclude: /\.entry\.js$/ */
  /* webpackExclude: /\.system\.entry\.js$/ */
  /* webpackMode: "lazy" */
  `./${l}.entry.js`).then((e => (u.set(l, e), e[o])), consoleError)
  /*!__STENCIL_STATIC_IMPORT_SWITCH__*/;
}, $ =  new Map, d = "undefined" != typeof window ? window : {}, m = d.document || {
  head: {}
}, p = {
  _$$flags$$_: 0,
  _$$resourcesUrl$$_: "",
  jmp: e => e(),
  raf: e => requestAnimationFrame(e),
  ael: (e, t, n, o) => e.addEventListener(t, n, o),
  rel: (e, t, n, o) => e.removeEventListener(t, n, o),
  ce: (e, t) => new CustomEvent(e, t)
}, promiseResolve = e => Promise.resolve(e), y =  (() => {
  try {
    return new CSSStyleSheet, "function" == typeof (new CSSStyleSheet).replaceSync;
  } catch (e) {}
  return !1;
})(), w = [], b = [], queueTask = (e, t) => n => {
  e.push(n), r || (r = !0, t && 4 /* PLATFORM_FLAGS.queueSync */ & p._$$flags$$_ ? nextTick(flush) : p.raf(flush));
}, consume = e => {
  for (let t = 0; t < e.length; t++) try {
    e[t](performance.now());
  } catch (e) {
    consoleError(e);
  }
  e.length = 0;
}, flush = () => {
  // always force a bunch of medium callbacks to run, but still have
  // a throttle on how many can run in a certain time
  // DOM READS!!!
  consume(w), consume(b), (r = w.length > 0) && 
  // still more to do yet, but we've run out of time
  // let's let this thing cool off and try again in the next tick
  p.raf(flush);
}, nextTick =  e => promiseResolve().then(e), g =  queueTask(b, !0);

export { bootstrapLazy as b, h, promiseResolve as p, registerInstance as r }