"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.runReleaseTasks = void 0;
const ansi_colors_1 = __importDefault(require("ansi-colors"));
const execa_1 = __importDefault(require("execa"));
const listr_1 = __importDefault(require("listr"));
const release_utils_1 = require("./utils/release-utils");
const validate_build_1 = require("./test/validate-build");
const license_1 = require("./license");
const build_1 = require("./build");
/**
 * Runs a litany of tasks used to ensure a safe release of a new version of Stencil
 * @param opts build options containing the metadata needed to release a new version of Stencil
 * @param args stringified arguments used to influence the release steps that are taken
 */
function runReleaseTasks(opts, args) {
    const rootDir = opts.rootDir;
    const pkg = opts.packageJson;
    const tasks = [];
    const newVersion = opts.version;
    const isDryRun = args.includes('--dry-run') || opts.version.includes('dryrun');
    if (isDryRun) {
        console.log(ansi_colors_1.default.bold.yellow(`\n  🏃‍ Dry Run!\n`));
    }
    if (!opts.isPublishRelease) {
        tasks.push({
            title: 'Validate version',
            task: () => {
                if (!(0, release_utils_1.isValidVersionInput)(opts.version)) {
                    throw new Error(`Version should be either ${release_utils_1.SEMVER_INCREMENTS.join(', ')}, or a valid semver version.`);
                }
            },
            skip: () => isDryRun,
        });
    }
    if (opts.isPublishRelease) {
        tasks.push({
            title: 'Check for pre-release version',
            task: () => {
                if (!pkg.private && (0, release_utils_1.isPrereleaseVersion)(newVersion) && !opts.tag) {
                    throw new Error('You must specify a dist-tag using --tag when publishing a pre-release version. This prevents accidentally tagging unstable versions as "latest". https://docs.npmjs.com/cli/dist-tag');
                }
            },
        });
    }
    if (!opts.isPublishRelease) {
        tasks.push({
            title: `Install npm dependencies ${ansi_colors_1.default.dim('(npm ci)')}`,
            task: () => (0, execa_1.default)('npm', ['ci'], { cwd: rootDir }),
        }, {
            title: `Transpile Stencil ${ansi_colors_1.default.dim('(tsc.prod)')}`,
            task: () => (0, execa_1.default)('npm', ['run', 'tsc.prod'], { cwd: rootDir }),
        }, {
            title: `Bundle @stencil/core ${ansi_colors_1.default.dim('(' + opts.buildId + ')')}`,
            task: () => (0, build_1.bundleBuild)(opts),
        }, 
        // {
        //   title: 'Run jest tests',
        //   task: () => execa('npm', ['run', 'test.jest'], { cwd: rootDir }),
        // },
        // {
        //   title: 'Run karma tests',
        //   task: () => execa('npm', ['run', 'test.karma.prod'], { cwd: rootDir }),
        // },
        {
            title: 'Build license',
            task: () => (0, license_1.createLicense)(rootDir),
        }, {
            title: 'Validate build',
            task: () => (0, validate_build_1.validateBuild)(rootDir),
        }, {
            title: `Set package.json version to ${ansi_colors_1.default.bold.yellow(opts.version)}`,
            task: async () => {
                // use `--no-git-tag-version` to ensure that the tag for the release is not prematurely created
                await (0, execa_1.default)('npm', ['version', '--no-git-tag-version', opts.version], { cwd: rootDir });
            },
        });
    }
    if (opts.isPublishRelease) {
        tasks.push({
            title: 'Publish @stencil/core to npm',
            task: () => {
                const cmd = 'npm';
                const cmdArgs = ['publish', '--otp', opts.otp].concat(opts.tag ? ['--tag', opts.tag] : []);
                if (isDryRun) {
                    return console.log(`[dry-run] ${cmd} ${cmdArgs.join(' ')}`);
                }
                return (0, execa_1.default)(cmd, cmdArgs, { cwd: rootDir });
            },
        }, {
            title: 'Tagging the latest git commit',
            task: () => {
                const cmd = 'git';
                const cmdArgs = ['tag', `v${opts.version}`];
                if (isDryRun) {
                    return console.log(`[dry-run] ${cmd} ${cmdArgs.join(' ')}`);
                }
                return (0, execa_1.default)(cmd, cmdArgs, { cwd: rootDir });
            },
        }, {
            title: 'Pushing git commits',
            task: () => {
                const cmd = 'git';
                const cmdArgs = ['push'];
                if (isDryRun) {
                    return console.log(`[dry-run] ${cmd} ${cmdArgs.join(' ')}`);
                }
                return (0, execa_1.default)(cmd, cmdArgs, { cwd: rootDir });
            },
        }, {
            title: 'Pushing git tags',
            task: () => {
                const cmd = 'git';
                const cmdArgs = ['push', '--tags'];
                if (isDryRun) {
                    return console.log(`[dry-run] ${cmd} ${cmdArgs.join(' ')}`);
                }
                return (0, execa_1.default)(cmd, cmdArgs, { cwd: rootDir });
            },
        });
    }
    if (opts.isPublishRelease) {
        tasks.push({
            title: 'Create Github Release',
            task: () => {
                return (0, release_utils_1.postGithubRelease)(opts);
            },
        });
    }
    const listr = new listr_1.default(tasks);
    listr
        .run()
        .then(() => {
        if (opts.isPublishRelease) {
            console.log(`\n ${opts.vermoji}  ${ansi_colors_1.default.bold.magenta(pkg.name)} ${ansi_colors_1.default.bold.yellow(newVersion)} published!! ${opts.vermoji}\n`);
        }
        else {
            console.log(`\n ${opts.vermoji}  ${ansi_colors_1.default.bold.magenta(pkg.name)} ${ansi_colors_1.default.bold.yellow(newVersion)} prepared, check the diffs and commit ${opts.vermoji}\n`);
        }
    })
        .catch((err) => {
        console.log(`\n🤒  ${ansi_colors_1.default.red(err)}\n`);
        console.log(err);
        process.exit(1);
    });
}
exports.runReleaseTasks = runReleaseTasks;
