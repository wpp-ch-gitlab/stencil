import type * as d from '../../../declarations';
export declare const validateOutputTargets: (config: d.ValidatedConfig, diagnostics: d.Diagnostic[]) => void;
