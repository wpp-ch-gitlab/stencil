import ts from 'typescript';
import type * as d from '../../declarations';
export declare const addImports: (transformOpts: d.TransformOptions, tsSourceFile: ts.SourceFile, importFnNames: string[], importPath: string) => ts.SourceFile;
