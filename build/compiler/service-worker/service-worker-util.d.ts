import type * as d from '../../declarations';
export declare const generateServiceWorkerUrl: (outputTarget: d.OutputTargetWww, serviceWorker: d.ServiceWorkerConfig) => string;
