import ts from 'typescript';
import type * as d from '../../declarations';
export declare const transformHostData: (classElements: ts.ClassElement[], moduleFile: d.Module) => void;
