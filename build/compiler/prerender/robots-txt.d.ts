import type * as d from '../../declarations';
export declare const generateRobotsTxt: (manager: d.PrerenderManager, sitemapResults: d.SitemapXmpResults) => Promise<d.RobotsTxtResults>;
