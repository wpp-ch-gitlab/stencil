/**
 * Strip out comments from some CSS
 *
 * @param input the string we'd like to de-comment
 * @returns de-commented CSS!
 */
export declare const stripCssComments: (input: string) => string;
