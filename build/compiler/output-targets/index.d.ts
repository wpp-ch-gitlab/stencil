import type * as d from '../../declarations';
export declare const generateOutputTargets: (config: d.ValidatedConfig, compilerCtx: d.CompilerCtx, buildCtx: d.BuildCtx) => Promise<void>;
