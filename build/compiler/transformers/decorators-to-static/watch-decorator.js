import { augmentDiagnosticWithNode, buildError, buildWarn, flatOne } from '@utils';
import ts from 'typescript';
import { convertValueToLiteral, createStaticGetter } from '../transform-utils';
import { getDeclarationParameters, isDecoratorNamed } from './decorator-utils';
export const watchDecoratorsToStatic = (config, diagnostics, decoratedProps, watchable, newMembers) => {
    const watchers = decoratedProps
        .filter(ts.isMethodDeclaration)
        .map((method) => parseWatchDecorator(config, diagnostics, watchable, method));
    const flatWatchers = flatOne(watchers);
    if (flatWatchers.length > 0) {
        newMembers.push(createStaticGetter('watchers', convertValueToLiteral(flatWatchers)));
    }
};
const parseWatchDecorator = (config, diagnostics, watchable, method) => {
    const methodName = method.name.getText();
    return method.decorators.filter(isDecoratorNamed('Watch')).map((decorator) => {
        const [propName] = getDeclarationParameters(decorator);
        if (!watchable.has(propName)) {
            const diagnostic = config.devMode ? buildWarn(diagnostics) : buildError(diagnostics);
            diagnostic.messageText = `@Watch('${propName}') is trying to watch for changes in a property that does not exist.
        Make sure only properties decorated with @State() or @Prop() are watched.`;
            augmentDiagnosticWithNode(diagnostic, decorator);
        }
        return {
            propName,
            methodName,
        };
    });
};
//# sourceMappingURL=watch-decorator.js.map