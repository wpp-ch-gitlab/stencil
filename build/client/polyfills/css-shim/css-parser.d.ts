/** @unrestricted */
export declare class StyleNode {
    start: number;
    end: number;
    previous: StyleNode | null;
    parent: StyleNode | null;
    rules: StyleNode[] | null;
    parsedCssText: string;
    cssText: string;
    atRule: boolean;
    type: number;
    keyframesName: string | undefined;
    selector: string;
    parsedSelector: string;
}
/**
 * Given a string of CSS, return a simple rule tree
 * @param text the CSS to generate a tree from
 * @returns the generated tree
 */
export declare function parse(text: string): StyleNode;
/**
 * Stringify some parsed CSS.
 * @param node the CSS root node to stringify
 * @param preserveProperties if `false`, custom CSS properties will be removed from the CSS. If `true`, they will be
 * preserved.
 * @param text an optional string to append the stringified CSS to
 * @returns the stringified CSS.
 */
export declare function stringify(node: StyleNode, preserveProperties: boolean, text?: string): string;
/**
 *
 * @param cssText the stringified CSS to remove custom properties from
 * @returns the sanitized CSS
 */
export declare function removeCustomPropAssignment(cssText: string): string;
/** @enum {number} */
export declare const types: {
    STYLE_RULE: number;
    KEYFRAMES_RULE: number;
    MEDIA_RULE: number;
    MIXIN_RULE: number;
};
