import { h } from '@wpp-open-stencil/core';
export class SiblingRoot {
  componentWillLoad() {
    return new Promise((resolve) => {
      setTimeout(resolve, 50);
    });
  }
  render() {
    return (h("div", null, h("section", null, "sibling-shadow-dom"), h("article", null, h("slot", null))));
  }
  static get is() { return "sibling-root"; }
  static get registryIs() { return "sibling-root"; }
  static get encapsulation() { return "scoped"; }
  static get styles() { return ":host {\n      display: block;\n      background: yellow;\n      color: maroon;\n      margin: 20px;\n      padding: 20px;\n    }\n    section {\n      background: blue;\n      color: white;\n    }\n    article {\n      background: maroon;\n      color: white;\n    }"; }
}
