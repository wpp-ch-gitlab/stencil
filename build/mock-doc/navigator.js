export class MockNavigator {
    constructor() {
        this.appCodeName = 'MockNavigator';
        this.appName = 'MockNavigator';
        this.appVersion = 'MockNavigator';
        this.platform = 'MockNavigator';
        this.userAgent = 'MockNavigator';
    }
}
//# sourceMappingURL=navigator.js.map