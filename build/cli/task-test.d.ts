import type { ValidatedConfig } from '../declarations';
/**
 * Entrypoint for any Stencil tests
 * @param config a validated Stencil configuration entity
 */
export declare const taskTest: (config: ValidatedConfig) => Promise<void>;
