import ts from 'typescript';
import type * as d from '../../declarations';
export declare const updateStyleImports: (transformOpts: d.TransformOptions, tsSourceFile: ts.SourceFile, moduleFile: d.Module) => ts.SourceFile;
