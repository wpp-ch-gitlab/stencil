import type * as d from '../../../declarations';
export declare const validateDocs: (config: d.ValidatedConfig, diagnostics: d.Diagnostic[], userOutputs: d.OutputTarget[]) => d.OutputTarget[];
