import type * as d from '../../declarations';
export declare function runtimeLogging(win: Window & typeof globalThis, opts: d.HydrateDocumentOptions, results: d.HydrateResults): void;
