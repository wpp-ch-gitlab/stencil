import ts from 'typescript';
import type * as d from '../../../declarations';
export declare const addNativeElementGetter: (classMembers: ts.ClassElement[], cmp: d.ComponentCompilerMeta) => void;
