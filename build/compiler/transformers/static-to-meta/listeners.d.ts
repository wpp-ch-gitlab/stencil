import ts from 'typescript';
import type * as d from '../../../declarations';
export declare const parseStaticListeners: (staticMembers: ts.ClassElement[]) => d.ComponentCompilerListener[];
