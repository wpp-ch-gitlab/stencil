import ts from 'typescript';
import type * as d from '../../../declarations';
export declare const elementDecoratorsToStatic: (diagnostics: d.Diagnostic[], decoratedMembers: ts.ClassElement[], typeChecker: ts.TypeChecker, newMembers: ts.ClassElement[]) => void;
