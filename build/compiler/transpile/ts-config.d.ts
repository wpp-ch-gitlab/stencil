import ts from 'typescript';
import type * as d from '../../declarations';
export declare const getTsOptionsToExtend: (config: d.ValidatedConfig) => ts.CompilerOptions;
