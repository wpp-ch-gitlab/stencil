import { isRemoteUrl, isString, normalizePath } from '@utils';
import { basename, dirname, isAbsolute, join, resolve } from 'path';
import ts from 'typescript';
import { version } from '../../../version';
import { IS_BROWSER_ENV, IS_NODE_ENV } from '../environment';
import { resolveRemoteModuleIdSync } from '../resolve/resolve-module-sync';
import { isDtsFile, isJsFile, isJsonFile, isJsxFile, isLocalModule, isStencilCoreImport, isTsFile, isTsxFile, } from '../resolve/resolve-utils';
import { patchTsSystemFileSystem } from './typescript-sys';
export const patchTypeScriptResolveModule = (config, inMemoryFs) => {
    let compilerExe;
    if (config.sys) {
        compilerExe = config.sys.getCompilerExecutingPath();
    }
    else if (IS_BROWSER_ENV) {
        compilerExe = location.href;
    }
    if (shouldPatchRemoteTypeScript(compilerExe)) {
        const resolveModuleName = (ts.__resolveModuleName = ts.resolveModuleName);
        ts.resolveModuleName = (moduleName, containingFile, compilerOptions, host, cache, redirectedReference) => {
            const resolvedModule = patchedTsResolveModule(config, inMemoryFs, moduleName, containingFile);
            if (resolvedModule) {
                return resolvedModule;
            }
            return resolveModuleName(moduleName, containingFile, compilerOptions, host, cache, redirectedReference);
        };
    }
};
export const tsResolveModuleName = (config, compilerCtx, moduleName, containingFile) => {
    const resolveModuleName = ts.__resolveModuleName || ts.resolveModuleName;
    if (moduleName && resolveModuleName && config.tsCompilerOptions) {
        const host = patchTsSystemFileSystem(config, config.sys, compilerCtx.fs, ts.sys);
        const compilerOptions = { ...config.tsCompilerOptions };
        compilerOptions.resolveJsonModule = true;
        return resolveModuleName(moduleName, containingFile, compilerOptions, host);
    }
    return null;
};
export const tsResolveModuleNamePackageJsonPath = (config, compilerCtx, moduleName, containingFile) => {
    try {
        const resolvedModule = tsResolveModuleName(config, compilerCtx, moduleName, containingFile);
        if (resolvedModule && resolvedModule.resolvedModule && resolvedModule.resolvedModule.resolvedFileName) {
            const rootDir = resolve('/');
            let resolvedFileName = resolvedModule.resolvedModule.resolvedFileName;
            for (let i = 0; i < 30; i++) {
                if (rootDir === resolvedFileName) {
                    return null;
                }
                resolvedFileName = dirname(resolvedFileName);
                const pkgJsonPath = join(resolvedFileName, 'package.json');
                const exists = config.sys.accessSync(pkgJsonPath);
                if (exists) {
                    return normalizePath(pkgJsonPath);
                }
            }
        }
    }
    catch (e) {
        config.logger.error(e);
    }
    return null;
};
export const patchedTsResolveModule = (config, inMemoryFs, moduleName, containingFile) => {
    if (isLocalModule(moduleName)) {
        const containingDir = dirname(containingFile);
        let resolvedFileName = join(containingDir, moduleName);
        resolvedFileName = normalizePath(ensureExtension(resolvedFileName, containingFile));
        if (isAbsolute(resolvedFileName) && !inMemoryFs.accessSync(resolvedFileName)) {
            return null;
        }
        if (!isAbsolute(resolvedFileName) && !resolvedFileName.startsWith('.') && !resolvedFileName.startsWith('/')) {
            resolvedFileName = './' + resolvedFileName;
        }
        const rtn = {
            resolvedModule: {
                extension: getTsResolveExtension(resolvedFileName),
                resolvedFileName,
                packageId: {
                    name: moduleName,
                    subModuleName: '',
                    version,
                },
            },
        };
        rtn.failedLookupLocations = [];
        return rtn;
    }
    // node module id
    return tsResolveNodeModule(config, inMemoryFs, moduleName, containingFile);
};
export const tsResolveNodeModule = (config, inMemoryFs, moduleId, containingFile) => {
    if (isStencilCoreImport(moduleId)) {
        const rtn = {
            resolvedModule: {
                extension: ts.Extension.Dts,
                resolvedFileName: normalizePath(config.sys.getLocalModulePath({
                    rootDir: config.rootDir,
                    moduleId: '@stencil/core',
                    path: 'internal/index.d.ts',
                })),
                packageId: {
                    name: moduleId,
                    subModuleName: '',
                    version,
                },
            },
        };
        rtn.failedLookupLocations = [];
        return rtn;
    }
    const resolved = resolveRemoteModuleIdSync(config, inMemoryFs, {
        moduleId,
        containingFile,
    });
    if (resolved) {
        const rtn = {
            resolvedModule: {
                extension: ts.Extension.Js,
                resolvedFileName: resolved.resolvedUrl,
                packageId: {
                    name: moduleId,
                    subModuleName: '',
                    version: resolved.packageJson.version,
                },
            },
        };
        rtn.failedLookupLocations = [];
        return rtn;
    }
    return null;
};
export const ensureExtension = (fileName, containingFile) => {
    if (!basename(fileName).includes('.') && isString(containingFile)) {
        containingFile = containingFile.toLowerCase();
        if (isJsFile(containingFile)) {
            fileName += '.js';
        }
        else if (isDtsFile(containingFile)) {
            fileName += '.d.ts';
        }
        else if (isTsxFile(containingFile)) {
            fileName += '.tsx';
        }
        else if (isTsFile(containingFile)) {
            fileName += '.ts';
        }
        else if (isJsxFile(containingFile)) {
            fileName += '.jsx';
        }
    }
    return fileName;
};
const getTsResolveExtension = (p) => {
    if (isDtsFile(p)) {
        return ts.Extension.Dts;
    }
    if (isTsxFile(p)) {
        return ts.Extension.Tsx;
    }
    if (isJsFile(p)) {
        return ts.Extension.Js;
    }
    if (isJsxFile(p)) {
        return ts.Extension.Jsx;
    }
    if (isJsonFile(p)) {
        return ts.Extension.Json;
    }
    return ts.Extension.Ts;
};
const shouldPatchRemoteTypeScript = (compilerExe) => !IS_NODE_ENV && isRemoteUrl(compilerExe);
//# sourceMappingURL=typescript-resolve-module.js.map