import type * as d from '../declarations';
export declare const getValue: (ref: d.RuntimeRef, propName: string) => any;
export declare const setValue: (ref: d.RuntimeRef, propName: string, newVal: any, cmpMeta: d.ComponentRuntimeMeta) => void;
