import type * as d from '../../../declarations';
export declare const getHydrateBuildConditionals: (cmps: d.ComponentCompilerMeta[]) => d.BuildConditionals;
