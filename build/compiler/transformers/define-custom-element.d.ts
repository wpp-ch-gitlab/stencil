import ts from 'typescript';
import type * as d from '../../declarations';
export declare const defineCustomElement: (tsSourceFile: ts.SourceFile, moduleFile: d.Module, transformOpts: d.TransformOptions) => ts.SourceFile;
