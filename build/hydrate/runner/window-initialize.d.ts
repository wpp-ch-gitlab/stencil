import type * as d from '../../declarations';
export declare function initializeWindow(win: Window & typeof globalThis, doc: Document, opts: d.HydrateDocumentOptions, results: d.HydrateResults): Window & typeof globalThis;
