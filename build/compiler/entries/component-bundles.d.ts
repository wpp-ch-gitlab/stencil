import type * as d from '../../declarations';
/**
 * Generate a list of all component tags that will be used by the output
 * @param config the Stencil configuration used for the build
 * @param defaultBundles metadata of the assumed components being used/bundled
 * @param allCmps all known components
 * @returns a set of all component tags that are used
 */
export declare function computeUsedComponents(config: d.ValidatedConfig, defaultBundles: readonly d.ComponentCompilerMeta[][], allCmps: readonly d.ComponentCompilerMeta[]): Set<string>;
/**
 * Generate the bundles that will be used during the bundling process
 * @param config the Stencil configuration used for the build
 * @param buildCtx the current build context
 * @returns the bundles to be used during the bundling process
 */
export declare function generateComponentBundles(config: d.ValidatedConfig, buildCtx: d.BuildCtx): readonly d.ComponentCompilerMeta[][];
