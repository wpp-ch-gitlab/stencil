import ts from 'typescript';
export declare const removeStaticMetaProperties: (classNode: ts.ClassDeclaration) => ts.ClassElement[];
