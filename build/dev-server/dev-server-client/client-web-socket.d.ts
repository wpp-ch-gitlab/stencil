import type * as d from '../../declarations';
export declare const initClientWebSocket: (win: d.DevClientWindow, config: d.DevClientConfig) => void;
