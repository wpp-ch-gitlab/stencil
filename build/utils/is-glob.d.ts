/**
 * Check if a string is a glob pattern (e.g. 'src/*.js' or something like that)
 *
 * @param str a string to check
 * @returns whether the string is a glob pattern or not
 */
export declare const isGlob: (str: string) => boolean;
