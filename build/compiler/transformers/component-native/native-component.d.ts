import ts from 'typescript';
import type * as d from '../../../declarations';
export declare const updateNativeComponentClass: (transformOpts: d.TransformOptions, classNode: ts.ClassDeclaration, moduleFile: d.Module, cmp: d.ComponentCompilerMeta) => ts.VariableStatement | ts.ClassDeclaration;
