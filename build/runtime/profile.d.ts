export declare const createTime: (fnName: string, tagName?: string) => () => void;
export declare const uniqueTime: (key: string, measureText: string) => () => void;
export declare const installDevTools: () => void;
