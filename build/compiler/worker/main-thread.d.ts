import { CompilerWorkerContext, WorkerMainController } from '../../declarations';
export declare const createWorkerMainContext: (workerCtrl: WorkerMainController) => CompilerWorkerContext;
