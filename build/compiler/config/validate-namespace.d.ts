import type * as d from '../../declarations';
export declare const validateNamespace: (c: d.UnvalidatedConfig, diagnostics: d.Diagnostic[]) => void;
export declare const validateDistNamespace: (config: d.UnvalidatedConfig, diagnostics: d.Diagnostic[]) => void;
