import type * as d from '../../declarations';
export declare const normalizeStyles: (tagName: string, componentFilePath: string, styles: d.StyleCompiler[]) => void;
