import type * as d from '../../../declarations';
export declare const generateHydrateApp: (config: d.ValidatedConfig, compilerCtx: d.CompilerCtx, buildCtx: d.BuildCtx, outputTargets: d.OutputTargetHydrate[]) => Promise<void>;
