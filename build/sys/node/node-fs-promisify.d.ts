/// <reference types="node" />
import fs from 'graceful-fs';
export declare const copyFile: typeof fs.copyFile.__promisify__;
export declare const mkdir: typeof fs.mkdir.__promisify__;
export declare const readdir: typeof fs.readdir.__promisify__;
export declare const readFile: typeof fs.readFile.__promisify__;
export declare const stat: typeof fs.stat.__promisify__;
