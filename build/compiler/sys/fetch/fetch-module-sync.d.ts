import type * as d from '../../../declarations';
import { InMemoryFileSystem } from '../in-memory-fs';
export declare const fetchModuleSync: (sys: d.CompilerSystem, inMemoryFs: InMemoryFileSystem, pkgVersions: Map<string, string>, url: string, filePath: string) => string;
export declare const fetchUrlSync: (url: string) => string;
