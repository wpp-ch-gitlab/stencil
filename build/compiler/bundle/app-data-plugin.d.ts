import type { Plugin } from 'rollup';
import type * as d from '../../declarations';
export declare const appDataPlugin: (config: d.Config, compilerCtx: d.CompilerCtx, buildCtx: d.BuildCtx, build: d.BuildConditionals, platform: 'client' | 'hydrate' | 'worker') => Plugin;
export declare const getGlobalScriptData: (config: d.Config, compilerCtx: d.CompilerCtx) => GlobalScript[];
interface GlobalScript {
    defaultName: string;
    path: string;
}
export {};
