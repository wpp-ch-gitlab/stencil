import type * as d from '../../declarations';
export declare const generateEs5DisabledMessage: (config: d.Config, compilerCtx: d.CompilerCtx, outputTarget: d.OutputTargetWww) => Promise<string>;
