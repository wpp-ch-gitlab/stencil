import type * as d from '../declarations';
export declare const getElement: (ref: any) => d.HostElement;
