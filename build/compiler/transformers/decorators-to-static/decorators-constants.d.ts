/**
 * Decorators on class declarations that we remove as part of the compilation
 * process
 */
export declare const CLASS_DECORATORS_TO_REMOVE: readonly ["Component"];
/**
 * Decorators on class members that we remove as part of the compilation
 * process
 */
export declare const MEMBER_DECORATORS_TO_REMOVE: readonly ["Element", "Event", "Listen", "Method", "Prop", "State", "Watch"];
/**
 * Decorators whose 'decorees' we need to rewrite during compilation from
 * class fields to instead initialize them in a constructor.
 */
export declare const CONSTRUCTOR_DEFINED_MEMBER_DECORATORS: readonly ["State", "Prop"];
