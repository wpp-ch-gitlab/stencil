import * as d from '@stencil/core/declarations';
/**
 * Generates a stub {@link TypesImportData}.
 * @param overrides a partial implementation of `TypesImportData`. Any provided fields will override the defaults
 * provided by this function.
 * @returns the stubbed `TypesImportData`
 */
export declare const stubTypesImportData: (overrides?: Partial<d.TypesImportData>) => d.TypesImportData;
