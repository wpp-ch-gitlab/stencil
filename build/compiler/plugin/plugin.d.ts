import type * as d from '../../declarations';
import { PluginCtx } from '../../declarations';
export declare const runPluginResolveId: (pluginCtx: PluginCtx, importee: string) => Promise<string>;
export declare const runPluginLoad: (pluginCtx: PluginCtx, id: string) => Promise<string>;
export declare const runPluginTransforms: (config: d.ValidatedConfig, compilerCtx: d.CompilerCtx, buildCtx: d.BuildCtx, id: string, cmp?: d.ComponentCompilerMeta) => Promise<d.PluginTransformResults>;
export declare const runPluginTransformsEsmImports: (config: d.Config, compilerCtx: d.CompilerCtx, buildCtx: d.BuildCtx, code: string, id: string) => Promise<d.PluginTransformResults>;
