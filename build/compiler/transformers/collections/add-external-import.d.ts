import type * as d from '../../../declarations';
export declare const addExternalImport: (config: d.Config, compilerCtx: d.CompilerCtx, buildCtx: d.BuildCtx, moduleFile: d.Module, containingFile: string, moduleId: string, resolveCollections: boolean) => void;
