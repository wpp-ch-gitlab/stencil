import ts from 'typescript';
import type * as d from '../../declarations';
export declare const getScriptTarget: () => ts.ScriptTarget;
/**
 * Determine if a class member is private or not
 * @param member the class member to evaluate
 * @returns `true` if the member has the `private` or `protected` modifier attached to it. `false` otherwise
 */
export declare const isMemberPrivate: (member: ts.ClassElement) => boolean;
/**
 * Convert a JavaScript value to the TypeScript Intermediate Representation
 * (IR) for a literal Abstract Syntax Tree (AST) node with that same value. The
 * value to convert may be a primitive type like `string`, `boolean`, etc or
 * may be an `Object`, `Array`, etc.
 *
 * Note that this function takes a param (`refs`) with a default value,
 * normally a value should _not_ be passed for this parameter since it is
 * intended to be used for recursive calls.
 *
 * @param val the value to convert
 * @param refs a set of references, used in recursive calls to avoid
 * circular references when creating object literal IR instances. **note that
 * you shouldn't pass this parameter unless you know what you're doing!**
 * @returns TypeScript IR for a literal corresponding to the JavaScript value
 * with which the function was called
 */
export declare const convertValueToLiteral: (val: any, refs?: WeakSet<any>) => ts.Identifier | ts.StringLiteral | ts.ObjectLiteralExpression | ts.ArrayLiteralExpression | ts.TrueLiteral | ts.FalseLiteral | ts.BigIntLiteral | ts.NumericLiteral;
/**
 * Create a TypeScript getter declaration AST node corresponding to a
 * supplied prop name and return value
 *
 * @param propName the name of the prop to access
 * @param returnExpression a TypeScript AST node to return from the getter
 * @returns an AST node representing a getter
 */
export declare const createStaticGetter: (propName: string, returnExpression: ts.Expression) => ts.GetAccessorDeclaration;
export declare const getStaticValue: (staticMembers: ts.ClassElement[], staticName: string) => any;
export declare const arrayLiteralToArray: (arr: ts.ArrayLiteralExpression) => any[];
export declare const objectLiteralToObjectMap: (objectLiteral: ts.ObjectLiteralExpression) => ObjectMap;
export declare class ObjectMap {
    [key: string]: ts.Expression | ObjectMap;
}
/**
 * Generate a series of type references for a given AST node
 * @param baseNode the AST node to pull type references from
 * @param sourceFile the source file in which the provided `baseNode` exists
 * @returns the generated series of type references
 */
export declare const getAttributeTypeInfo: (baseNode: ts.Node, sourceFile: ts.SourceFile) => d.ComponentCompilerTypeReferences;
export declare const validateReferences: (diagnostics: d.Diagnostic[], references: d.ComponentCompilerTypeReferences, node: ts.Node) => void;
export declare const resolveType: (checker: ts.TypeChecker, type: ts.Type) => string;
/**
 * Formats a TypeScript `Type` entity as a string
 * @param checker a reference to the TypeScript type checker
 * @param type a TypeScript `Type` entity to format
 * @returns the formatted string
 */
export declare const typeToString: (checker: ts.TypeChecker, type: ts.Type) => string;
export declare const parseDocsType: (checker: ts.TypeChecker, type: ts.Type, parts: Set<string>) => void;
export declare const getModuleFromSourceFile: (compilerCtx: d.CompilerCtx, tsSourceFile: ts.SourceFile) => d.Module;
export declare const getComponentMeta: (compilerCtx: d.CompilerCtx, tsSourceFile: ts.SourceFile, node: ts.ClassDeclaration) => d.ComponentCompilerMeta;
export declare const getComponentTagName: (staticMembers: ts.ClassElement[]) => string;
export declare const getComponentRegistryTagName: (staticMembers: ts.ClassElement[]) => string;
export declare const isStaticGetter: (member: ts.ClassElement) => boolean;
/**
 * Create a serialized representation of a TypeScript `Symbol` entity to expose the Symbol's text and attached JSDoc.
 * Note that the `Symbol` being serialized is not the same as the JavaScript primitive 'symbol'.
 * @param checker a reference to the TypeScript type checker
 * @param symbol the `Symbol` to serialize
 * @returns the serialized `Symbol` data
 */
export declare const serializeSymbol: (checker: ts.TypeChecker, symbol: ts.Symbol) => d.CompilerJsDoc;
/**
 * Maps a TypeScript 4.3+ JSDocTagInfo to a flattened Stencil CompilerJsDocTagInfo.
 * @param tags A readonly array of JSDocTagInfo objects.
 * @returns An array of CompilerJsDocTagInfo objects.
 */
export declare const mapJSDocTagInfo: (tags: readonly ts.JSDocTagInfo[]) => d.CompilerJsDocTagInfo[];
export declare const serializeDocsSymbol: (checker: ts.TypeChecker, symbol: ts.Symbol) => string;
export declare const isInternal: (jsDocs: d.CompilerJsDoc | undefined) => boolean;
export declare const isMethod: (member: ts.ClassElement, methodName: string) => member is ts.MethodDeclaration;
export declare const createImportStatement: (importFnNames: string[], importPath: string) => ts.ImportDeclaration;
export declare const createRequireStatement: (importFnNames: string[], importPath: string) => ts.VariableStatement;
export interface ConvertIdentifier {
    __identifier: boolean;
    __escapedText: string;
}
