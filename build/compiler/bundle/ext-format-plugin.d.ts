import type { Plugin } from 'rollup';
import type * as d from '../../declarations';
export declare const extFormatPlugin: (config: d.Config) => Plugin;
