import type * as d from '../../declarations';
export declare const outputLazyLoader: (config: d.ValidatedConfig, compilerCtx: d.CompilerCtx) => Promise<void>;
