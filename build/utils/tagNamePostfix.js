import ts from 'typescript';
import { H } from '../compiler/transformers/core-runtime-apis';
export const addTagNamePostfix = (tagName, postfix) => {
    if (postfix) {
        return `${tagName}-${postfix}`;
    }
    return tagName;
};
export const isTargetWebComponent = (tagName) => tagName.startsWith('wpp-');
export const convertTagNames = (config) => {
    config.logger.debug('==> Updating components JSX');
    return (transformCtx) => {
        const visit = (node) => {
            var _a, _b;
            if (ts.isClassDeclaration(node)) {
                config.logger.debug(`==> Class '${(_a = node.name) === null || _a === void 0 ? void 0 : _a.text}'`);
                return visitDeclaration(config, node, transformCtx);
            }
            else if (ts.isFunctionDeclaration(node)) {
                config.logger.debug(`==> Function '${(_b = node.name) === null || _b === void 0 ? void 0 : _b.text}'`);
                return visitDeclaration(config, node, transformCtx);
            }
            else if (ts.isArrowFunction(node)) {
                config.logger.debug(`==> Arrow function`);
                return visitDeclaration(config, node, transformCtx);
            }
            return ts.visitEachChild(node, visit, transformCtx);
        };
        return (tsSourceFile) => {
            return ts.visitEachChild(tsSourceFile, visit, transformCtx);
        };
    };
};
const visitDeclaration = (config, node, transformCtx) => {
    let isUpdateLogged = false;
    const visitNode = (node) => {
        if (ts.isCallExpression(node)) {
            const updatedNode = transformCtx.factory.createCallExpression(node.expression, node.typeArguments, [...node.arguments]);
            const hasUpdates = updateCallExpression(config, updatedNode);
            if (hasUpdates) {
                if (!isUpdateLogged) {
                    config.logger.debug(`- Updated component usage`);
                    isUpdateLogged = true;
                }
                transformCtx.factory.updateCallExpression(node, updatedNode.expression, updatedNode.typeArguments, updatedNode.arguments);
            }
        }
        node.forEachChild(visitNode);
    };
    visitNode(node);
    return node;
};
export const updateCallExpression = (config, node) => {
    if (node.arguments != null && node.arguments.length > 0) {
        if (ts.isIdentifier(node.expression)) {
            // h('tag')
            return visitCallExpressionArgs(config, node.expression, node.arguments);
        }
        else if (ts.isPropertyAccessExpression(node.expression)) {
            // document.createElement('tag')
            const n = node.expression.name;
            if (ts.isIdentifier(n) && n) {
                return visitCallExpressionArgs(config, n, node.arguments);
            }
        }
    }
    return false;
};
const visitCallExpressionArgs = (config, callExpressionName, args) => {
    const fnName = callExpressionName.escapedText;
    if (fnName === 'h' || fnName === H || fnName === 'createElement') {
        return visitCallExpressionArg(config, args[0]);
    }
    else if (args.length > 1 && fnName === 'createElementNS') {
        return visitCallExpressionArg(config, args[1]);
    }
    return false;
};
const visitCallExpressionArg = (config, arg) => {
    var _a;
    if (ts.isStringLiteral(arg)) {
        let tag = arg.text;
        if (typeof tag === 'string') {
            tag = tag.toLowerCase();
            if (isTargetWebComponent(tag)) {
                tag = addTagNamePostfix(tag, (_a = config.flags) === null || _a === void 0 ? void 0 : _a.tagNamePostfix);
                arg.text = tag;
                return true;
            }
        }
    }
    return false;
};
//# sourceMappingURL=tagNamePostfix.js.map