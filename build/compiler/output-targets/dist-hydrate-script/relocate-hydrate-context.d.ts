import type * as d from '../../../declarations';
export declare const relocateHydrateContextConst: (config: d.Config, compilerCtx: d.CompilerCtx, code: string) => string;
