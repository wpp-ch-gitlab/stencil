import { isOutputTargetDistCollection } from '../../output-targets/output-utils';
import { getAbsolutePath } from '../config-utils';
/**
 * Validate and return DIST_COLLECTION output targets, ensuring that the `dir`
 * property is set on them.
 *
 * @param config a validated configuration object
 * @param userOutputs an array of output targets
 * @returns an array of validated DIST_COLLECTION output targets
 */
export const validateCollection = (config, userOutputs) => {
    return userOutputs.filter(isOutputTargetDistCollection).map((outputTarget) => {
        var _a, _b;
        return {
            ...outputTarget,
            transformAliasedImportPaths: (_a = outputTarget.transformAliasedImportPaths) !== null && _a !== void 0 ? _a : false,
            dir: getAbsolutePath(config, (_b = outputTarget.dir) !== null && _b !== void 0 ? _b : 'dist/collection'),
        };
    });
};
//# sourceMappingURL=validate-collection.js.map