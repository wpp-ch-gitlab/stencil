import type * as d from '../../../declarations';
export declare const validateCustomElementBundle: (config: d.ValidatedConfig, userOutputs: d.OutputTarget[]) => (d.OutputTargetCopy | d.OutputTargetDistCustomElementsBundle)[];
