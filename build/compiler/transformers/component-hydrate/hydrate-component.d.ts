import ts from 'typescript';
import type * as d from '../../../declarations';
export declare const updateHydrateComponentClass: (classNode: ts.ClassDeclaration, moduleFile: d.Module, cmp: d.ComponentCompilerMeta) => ts.ClassDeclaration;
