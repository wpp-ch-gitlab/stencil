/**
 * Used to learn the size of a string in bytes.
 *
 * @param str The string to measure
 * @returns number
 */
export declare const byteSize: (str: string) => number;
