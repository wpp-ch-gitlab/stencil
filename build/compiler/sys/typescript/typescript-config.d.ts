import type * as d from '../../../declarations';
export declare const validateTsConfig: (config: d.Config, sys: d.CompilerSystem, init: d.LoadConfigInit) => Promise<{
    path: string;
    compilerOptions: any;
    files: string[];
    include: string[];
    exclude: string[];
    extends: string;
    diagnostics: d.Diagnostic[];
}>;
