import type * as d from '../../declarations';
export declare const generateSitemapXml: (manager: d.PrerenderManager) => Promise<d.SitemapXmpResults>;
export declare const getSitemapUrls: (manager: d.PrerenderManager) => string[];
