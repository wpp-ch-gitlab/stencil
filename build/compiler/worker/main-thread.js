export const createWorkerMainContext = (workerCtrl) => ({
    optimizeCss: workerCtrl.handler('optimizeCss'),
    prepareModule: workerCtrl.handler('prepareModule'),
    prerenderWorker: workerCtrl.handler('prerenderWorker'),
    transformCssToEsm: workerCtrl.handler('transformCssToEsm'),
});
//# sourceMappingURL=main-thread.js.map