import type * as d from '../declarations';
export declare const initializeClientHydrate: (hostElm: d.HostElement, tagName: string, hostId: string, hostRef: d.HostRef) => void;
export declare const initializeDocumentHydrate: (node: d.RenderNode, orgLocNodes: Map<string, any>) => void;
