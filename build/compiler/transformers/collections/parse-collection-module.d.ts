import type * as d from '../../../declarations';
export declare const parseCollection: (config: d.Config, compilerCtx: d.CompilerCtx, buildCtx: d.BuildCtx, moduleId: string, pkgJsonFilePath: string, pkgData: d.PackageJsonData) => d.CollectionCompilerMeta;
