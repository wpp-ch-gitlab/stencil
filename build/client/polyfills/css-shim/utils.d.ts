export declare const GLOBAL_SCOPE = ":root";
export declare function findRegex(regex: RegExp, cssText: string, offset: number): {
    start: number;
    end: number;
};
