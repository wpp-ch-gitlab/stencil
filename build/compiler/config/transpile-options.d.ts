import type { Config, ImportData, TransformCssToEsmInput, TransformOptions, TranspileOptions, TranspileResults } from '../../declarations';
export declare const getTranspileResults: (code: string, input: TranspileOptions) => {
    importData: ImportData;
    results: TranspileResults;
};
export declare const getTranspileConfig: (input: TranspileOptions) => {
    compileOpts: TranspileOptions;
    config: Config;
    transformOpts: TransformOptions;
};
export declare const getTranspileCssConfig: (compileOpts: TranspileOptions, importData: ImportData, results: TranspileResults) => TransformCssToEsmInput;
