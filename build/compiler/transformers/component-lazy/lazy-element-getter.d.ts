import ts from 'typescript';
import type * as d from '../../../declarations';
export declare const addLazyElementGetter: (classMembers: ts.ClassElement[], moduleFile: d.Module, cmp: d.ComponentCompilerMeta) => void;
