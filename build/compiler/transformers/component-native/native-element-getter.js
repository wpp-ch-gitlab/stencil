import ts from 'typescript';
export const addNativeElementGetter = (classMembers, cmp) => {
    // @Element() element;
    // is transformed into:
    // get element() { return this; }
    if (cmp.elementRef) {
        classMembers.push(ts.factory.createGetAccessorDeclaration(undefined, undefined, cmp.elementRef, [], undefined, ts.factory.createBlock([ts.factory.createReturnStatement(ts.factory.createThis())])));
    }
};
//# sourceMappingURL=native-element-getter.js.map