import ts from 'typescript';
import { stubComponentCompilerMeta } from '../../../compiler/types/tests/ComponentCompilerMeta.stub';
import * as FormatComponentRuntimeMeta from '../../../utils/format-component-runtime-meta';
import { createAnonymousClassMetadataProxy } from '../add-component-meta-proxy';
import { HTML_ELEMENT } from '../core-runtime-apis';
import * as TransformUtils from '../transform-utils';
describe('add-component-meta-proxy', () => {
    describe('createAnonymousClassMetadataProxy()', () => {
        let classExpr;
        let htmlElementHeritageClause;
        let literalMetadata;
        let formatComponentRuntimeMetaSpy;
        let convertValueToLiteralSpy;
        beforeEach(() => {
            htmlElementHeritageClause = ts.factory.createHeritageClause(ts.SyntaxKind.ExtendsKeyword, [
                ts.factory.createExpressionWithTypeArguments(ts.factory.createIdentifier(HTML_ELEMENT), []),
            ]);
            classExpr = ts.factory.createClassExpression(undefined, undefined, 'MyComponent', undefined, [htmlElementHeritageClause], undefined);
            literalMetadata = ts.factory.createStringLiteral('MyComponent');
            formatComponentRuntimeMetaSpy = jest.spyOn(FormatComponentRuntimeMeta, 'formatComponentRuntimeMeta');
            formatComponentRuntimeMetaSpy.mockImplementation((_compilerMeta, _includeMethods) => [0, 'tag-name', 'tag-name']);
            convertValueToLiteralSpy = jest.spyOn(TransformUtils, 'convertValueToLiteral');
            convertValueToLiteralSpy.mockImplementation((_compactMeta) => literalMetadata);
        });
        afterEach(() => {
            formatComponentRuntimeMetaSpy.mockRestore();
            convertValueToLiteralSpy.mockRestore();
        });
        it('returns a call expression', () => {
            const result = createAnonymousClassMetadataProxy(stubComponentCompilerMeta(), classExpr);
            expect(ts.isCallExpression(result)).toBe(true);
        });
        it('wraps the initializer in PROXY_CUSTOM_ELEMENT', () => {
            const result = createAnonymousClassMetadataProxy(stubComponentCompilerMeta(), classExpr);
            expect(result.expression.escapedText).toBe('___stencil_proxyCustomElement');
        });
        it("doesn't add any type arguments to the call", () => {
            const result = createAnonymousClassMetadataProxy(stubComponentCompilerMeta(), classExpr);
            expect(result.typeArguments).toHaveLength(0);
        });
        it('adds the correct arguments to the PROXY_CUSTOM_ELEMENT call', () => {
            const result = createAnonymousClassMetadataProxy(stubComponentCompilerMeta(), classExpr);
            expect(result.arguments).toHaveLength(2);
            expect(result.arguments[0]).toBe(classExpr);
            expect(result.arguments[1]).toBe(literalMetadata);
        });
        it('includes the heritage clause', () => {
            const result = createAnonymousClassMetadataProxy(stubComponentCompilerMeta(), classExpr);
            expect(result.arguments.length).toBeGreaterThanOrEqual(1);
            const createdClassExpression = result.arguments[0];
            expect(ts.isClassExpression(createdClassExpression)).toBe(true);
            expect(createdClassExpression.heritageClauses).toHaveLength(1);
            expect(createdClassExpression.heritageClauses[0]).toBe(htmlElementHeritageClause);
        });
    });
});
//# sourceMappingURL=add-component-meta-proxy.spec.js.map