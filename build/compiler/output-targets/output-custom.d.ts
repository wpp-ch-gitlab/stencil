import type * as d from '../../declarations';
export declare const outputCustom: (config: d.ValidatedConfig, compilerCtx: d.CompilerCtx, buildCtx: d.BuildCtx, docs: d.JsonDocs, outputTargets: d.OutputTarget[]) => Promise<void>;
