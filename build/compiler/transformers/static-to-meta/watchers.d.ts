import ts from 'typescript';
import type * as d from '../../../declarations';
export declare const parseStaticWatchers: (staticMembers: ts.ClassElement[]) => d.ComponentCompilerWatch[];
