import ts from 'typescript';
import type * as d from '../../../declarations';
export declare const addNativeComponentMeta: (classMembers: ts.ClassElement[], cmp: d.ComponentCompilerMeta) => void;
