import type { ValidatedConfig } from '../declarations';
import type { CoreCompiler } from './load-compiler';
export declare const taskDocs: (coreCompiler: CoreCompiler, config: ValidatedConfig) => Promise<void>;
