import ts from 'typescript';
import type * as d from '../../../declarations';
export declare const parseCallExpression: (m: d.Module | d.ComponentCompilerMeta, node: ts.CallExpression) => void;
