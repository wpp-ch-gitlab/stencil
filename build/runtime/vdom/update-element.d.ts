import type * as d from '../../declarations';
export declare const updateElement: (oldVnode: d.VNode | null, newVnode: d.VNode, isSvgMode: boolean, memberName?: string) => void;
