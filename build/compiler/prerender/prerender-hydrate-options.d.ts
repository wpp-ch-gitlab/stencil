import type * as d from '../../declarations';
export declare const getHydrateOptions: (prerenderConfig: d.PrerenderConfig, url: URL, diagnostics: d.Diagnostic[]) => d.PrerenderHydrateOptions;
