import ts from 'typescript';
import type * as d from '../../declarations';
export declare const createTsWatchProgram: (config: d.ValidatedConfig, buildCallback: (tsBuilder: ts.BuilderProgram) => Promise<void>) => Promise<{
    program: ts.WatchOfConfigFile<ts.EmitAndSemanticDiagnosticsBuilderProgram>;
    rebuild: () => void;
}>;
