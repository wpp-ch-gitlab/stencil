import type * as d from '../../../declarations';
export declare const generateHashedCopy: (config: d.Config, compilerCtx: d.CompilerCtx, path: string) => Promise<string>;
