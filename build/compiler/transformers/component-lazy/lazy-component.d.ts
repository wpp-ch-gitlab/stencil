import ts from 'typescript';
import type * as d from '../../../declarations';
export declare const updateLazyComponentClass: (transformOpts: d.TransformOptions, styleStatements: ts.Statement[], classNode: ts.ClassDeclaration, moduleFile: d.Module, cmp: d.ComponentCompilerMeta) => ts.VariableStatement | ts.ClassDeclaration;
