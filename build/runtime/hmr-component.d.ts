import type * as d from '../declarations';
export declare const hmrStart: (elm: d.HostElement, cmpMeta: d.ComponentRuntimeMeta, hmrVersionId: string) => void;
export declare const hmrFinish: (_elm: d.HostElement, _cmpMeta: d.ComponentRuntimeMeta) => void;
