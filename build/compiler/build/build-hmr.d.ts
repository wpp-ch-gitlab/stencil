import type * as d from '../../declarations';
export declare const generateHmr: (config: d.Config, compilerCtx: d.CompilerCtx, buildCtx: d.BuildCtx) => d.HotModuleReplacement;
