export declare class SiblingRoot {
  componentWillLoad(): Promise<unknown>;
  render(): any;
}
