import ts from 'typescript';
import type * as d from '../../../declarations';
export declare const styleToStatic: (newMembers: ts.ClassElement[], componentOptions: d.ComponentOptions) => void;
