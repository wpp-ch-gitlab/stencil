import type * as d from '../../declarations';
export declare function inspectElement(results: d.HydrateResults, elm: Element, depth: number): void;
