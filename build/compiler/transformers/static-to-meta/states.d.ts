import ts from 'typescript';
import type * as d from '../../../declarations';
export declare const parseStaticStates: (staticMembers: ts.ClassElement[]) => d.ComponentCompilerState[];
