export declare const HYDRATE_APP_CLOSURE_START = "/*hydrateAppClosure start*/";
export declare const HYDRATE_FACTORY_INTRO: string;
export declare const HYDRATE_FACTORY_OUTRO = "\n    /*hydrateAppClosure end*/\n    hydrateApp(window, $stencilHydrateOpts, $stencilHydrateResults, $stencilAfterHydrate, $stencilHydrateResolve);\n  }\n\n  hydrateAppClosure($stencilWindow);\n}\n";
