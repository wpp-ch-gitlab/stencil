import type * as d from '../../../declarations';
export declare const getLazyBuildConditionals: (config: d.ValidatedConfig, cmps: d.ComponentCompilerMeta[]) => d.BuildConditionals;
