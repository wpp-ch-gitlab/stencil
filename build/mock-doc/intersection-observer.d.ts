export declare class MockIntersectionObserver {
    constructor();
    disconnect(): void;
    observe(): void;
    takeRecords(): any[];
    unobserve(): void;
}
