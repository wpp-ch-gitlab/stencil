import type * as d from '../../../declarations';
export declare const slotsToMarkdown: (slots: d.JsonDocsSlot[]) => string[];
