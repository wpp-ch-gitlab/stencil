import type * as d from '../declarations';
export declare function initServerProcess(sendMsg: d.DevServerSendMessage): (msg: d.DevServerMessage) => void;
