import type * as d from '../../declarations';
export declare const updateGlobalStylesLink: (config: d.Config, doc: Document, globalScriptFilename: string, outputTarget: d.OutputTargetWww) => void;
