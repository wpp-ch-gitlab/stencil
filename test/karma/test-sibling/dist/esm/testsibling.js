import { p as promiseResolve, b as bootstrapLazy } from './index-bd1cc1b9.js';

/*
 Stencil Client Patch Browser v2.19.0 | MIT Licensed | https://stenciljs.com
 */
const patchBrowser = () => {
    const importMeta = import.meta.url;
    const opts = {};
    if (importMeta !== '') {
        opts.resourcesUrl = new URL('.', importMeta).href;
    }
    return promiseResolve(opts);
};

patchBrowser().then(options => {
  return bootstrapLazy([["sibling-root",[[6,"sibling-root","sibling-root"]]]], options);
});
