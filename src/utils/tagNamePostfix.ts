import ts from 'typescript';
import { H } from '../compiler/transformers/core-runtime-apis';
import * as d from '@stencil/core/declarations';

export const addTagNamePostfix = (tagName: string, postfix?: string) => {
  if (postfix) {
    return `${tagName}-${postfix}`;
  }

  return tagName;
};

export const isTargetWebComponent = (tagName: string) => tagName.startsWith('wpp-');

export const convertTagNames = (config: d.Config): ts.TransformerFactory<ts.SourceFile> => {
  config.logger.debug('==> Updating components JSX');

  return (transformCtx) => {
    const visit = (node: ts.Node): ts.VisitResult<ts.Node> => {
      if (ts.isClassDeclaration(node)) {
        config.logger.debug(`==> Class '${node.name?.text}'`);
        return visitDeclaration(config, node, transformCtx);
      } else if (ts.isFunctionDeclaration(node)) {
        config.logger.debug(`==> Function '${node.name?.text}'`);
        return visitDeclaration(config, node, transformCtx);
      } else if (ts.isArrowFunction(node)) {
        config.logger.debug(`==> Arrow function`);
        return visitDeclaration(config, node, transformCtx);
      }
      return ts.visitEachChild(node, visit, transformCtx);
    };

    return (tsSourceFile) => {
      return ts.visitEachChild(tsSourceFile, visit, transformCtx);
    };
  };
};

const visitDeclaration = (config: d.Config, node: ts.Node, transformCtx: ts.TransformationContext) => {
  let isUpdateLogged = false;
  const visitNode = (node: ts.Node) => {
    if (ts.isCallExpression(node)) {
      const updatedNode = transformCtx.factory.createCallExpression(node.expression, node.typeArguments, [...node.arguments]);
      const hasUpdates = updateCallExpression(config, updatedNode);

      if (hasUpdates) {
        if (!isUpdateLogged) {
          config.logger.debug(`- Updated component usage`);
          isUpdateLogged = true;
        }
        transformCtx.factory.updateCallExpression(node, updatedNode.expression, updatedNode.typeArguments, updatedNode.arguments);
      }
    }
    node.forEachChild(visitNode);
  };

  visitNode(node);

  return node;
};

export const updateCallExpression = (config: d.Config, node: ts.CallExpression): boolean => {
  if (node.arguments != null && node.arguments.length > 0) {
    if (ts.isIdentifier(node.expression)) {
      // h('tag')
      return visitCallExpressionArgs(config, node.expression, node.arguments);
    } else if (ts.isPropertyAccessExpression(node.expression)) {
      // document.createElement('tag')
      const n = node.expression.name;
      if (ts.isIdentifier(n) && n) {
        return visitCallExpressionArgs(config, n, node.arguments);
      }
    }
  }

  return false;
};

const visitCallExpressionArgs = (
  config: d.Config,
  callExpressionName: ts.Identifier,
  args: ts.NodeArray<ts.Expression>
): boolean => {
  const fnName = callExpressionName.escapedText as string;

  if (fnName === 'h' || fnName === H || fnName === 'createElement') {
    return visitCallExpressionArg(config, args[0]);
  } else if (args.length > 1 && fnName === 'createElementNS') {
    return visitCallExpressionArg(config, args[1]);
  }

  return false;
};

const visitCallExpressionArg = (config: d.Config, arg: ts.Expression): boolean => {
  if (ts.isStringLiteral(arg)) {
    let tag = arg.text;

    if (typeof tag === 'string') {
      tag = tag.toLowerCase();

      if (isTargetWebComponent(tag)) {
        tag = addTagNamePostfix(tag, config.flags?.tagNamePostfix);
        arg.text = tag;

        return true;
      }
    }
  }

  return false;
};
