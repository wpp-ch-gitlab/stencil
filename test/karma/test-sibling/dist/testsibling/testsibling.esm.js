import { p as r, b as o } from "./p-a42e9321.js";

/*
 Stencil Client Patch Browser v2.19.0 | MIT Licensed | https://stenciljs.com
 */ (() => {
  const o = import.meta.url, e = {};
  return "" !== o && (e.resourcesUrl = new URL(".", o).href), r(e);
})().then((r => o([ [ "p-67f66dba", [ [ 6, "sibling-root", "sibling-root" ] ] ] ], r)));