import type * as d from '../../declarations';
export declare const COMPONENTS_DTS_HEADER = "/* eslint-disable */\n/* tslint:disable */\n/**\n * This is an autogenerated file created by the Stencil compiler.\n * It contains typing information for all components that exist in this project.\n */";
export declare const sortImportNames: (a: d.TypesMemberNameData, b: d.TypesMemberNameData) => 1 | -1 | 0;
