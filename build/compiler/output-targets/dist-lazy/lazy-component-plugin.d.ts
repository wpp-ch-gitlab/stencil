import type { Plugin } from 'rollup';
import type * as d from '../../../declarations';
export declare const lazyComponentPlugin: (buildCtx: d.BuildCtx) => Plugin;
