import type * as d from '../../declarations';
export declare const createWorkerContext: (sys: d.CompilerSystem) => d.CompilerWorkerContext;
export declare const createWorkerMessageHandler: (sys: d.CompilerSystem) => d.WorkerMsgHandler;
