import type * as d from '../../declarations';
export declare const relativeImport: (pathFrom: string, pathTo: string, ext?: string, addPrefix?: boolean) => string;
export declare const getComponentsDtsSrcFilePath: (config: d.Config) => string;
export declare const getComponentsDtsTypesFilePath: (outputTarget: d.OutputTargetDist | d.OutputTargetDistTypes) => string;
export declare const isOutputTargetDist: (o: d.OutputTarget) => o is d.OutputTargetDist;
export declare const isOutputTargetDistCollection: (o: d.OutputTarget) => o is d.OutputTargetDistCollection;
export declare const isOutputTargetDistCustomElements: (o: d.OutputTarget) => o is d.OutputTargetDistCustomElements;
export declare const isOutputTargetDistCustomElementsBundle: (o: d.OutputTarget) => o is d.OutputTargetDistCustomElementsBundle;
export declare const isOutputTargetCopy: (o: d.OutputTarget) => o is d.OutputTargetCopy;
export declare const isOutputTargetDistLazy: (o: d.OutputTarget) => o is d.OutputTargetDistLazy;
export declare const isOutputTargetAngular: (o: d.OutputTarget) => o is d.OutputTargetAngular;
export declare const isOutputTargetDistLazyLoader: (o: d.OutputTarget) => o is d.OutputTargetDistLazyLoader;
export declare const isOutputTargetDistGlobalStyles: (o: d.OutputTarget) => o is d.OutputTargetDistGlobalStyles;
export declare const isOutputTargetHydrate: (o: d.OutputTarget) => o is d.OutputTargetHydrate;
export declare const isOutputTargetCustom: (o: d.OutputTarget) => o is d.OutputTargetCustom;
export declare const isOutputTargetDocs: (o: d.OutputTarget) => o is d.OutputTargetDocsJson | d.OutputTargetDocsCustom | d.OutputTargetDocsReadme | d.OutputTargetDocsVscode;
export declare const isOutputTargetDocsReadme: (o: d.OutputTarget) => o is d.OutputTargetDocsReadme;
export declare const isOutputTargetDocsJson: (o: d.OutputTarget) => o is d.OutputTargetDocsJson;
export declare const isOutputTargetDocsCustom: (o: d.OutputTarget) => o is d.OutputTargetDocsCustom;
export declare const isOutputTargetDocsVscode: (o: d.OutputTarget) => o is d.OutputTargetDocsVscode;
export declare const isOutputTargetWww: (o: d.OutputTarget) => o is d.OutputTargetWww;
export declare const isOutputTargetStats: (o: d.OutputTarget) => o is d.OutputTargetStats;
export declare const isOutputTargetDistTypes: (o: d.OutputTarget) => o is d.OutputTargetDistTypes;
export declare const getComponentsFromModules: (moduleFiles: d.Module[]) => d.ComponentCompilerMeta[];
export declare const ANGULAR = "angular";
export declare const COPY = "copy";
export declare const CUSTOM = "custom";
export declare const DIST = "dist";
export declare const DIST_COLLECTION = "dist-collection";
export declare const DIST_CUSTOM_ELEMENTS = "dist-custom-elements";
export declare const DIST_CUSTOM_ELEMENTS_BUNDLE = "dist-custom-elements-bundle";
export declare const DIST_TYPES = "dist-types";
export declare const DIST_HYDRATE_SCRIPT = "dist-hydrate-script";
export declare const DIST_LAZY = "dist-lazy";
export declare const DIST_LAZY_LOADER = "dist-lazy-loader";
export declare const DIST_GLOBAL_STYLES = "dist-global-styles";
export declare const DOCS_CUSTOM = "docs-custom";
export declare const DOCS_JSON = "docs-json";
export declare const DOCS_README = "docs-readme";
export declare const DOCS_VSCODE = "docs-vscode";
export declare const STATS = "stats";
export declare const WWW = "www";
/**
 * Valid output targets to specify in a Stencil config.
 *
 * Note that there are some output targets (e.g. `DIST_TYPES`) which are
 * programmatically set as output targets by the compiler when other output
 * targets (in that case `DIST`) are set, but which are _not_ supported in a
 * Stencil config. This is enforced in the output target validation code.
 */
export declare const VALID_CONFIG_OUTPUT_TARGETS: readonly ["www", "dist", "dist-collection", "dist-custom-elements", "dist-custom-elements-bundle", "dist-lazy", "dist-hydrate-script", "docs-json", "docs-readme", "docs-vscode", "docs-custom", "angular", "copy", "custom", "stats"];
declare type ValidConfigOutputTarget = typeof VALID_CONFIG_OUTPUT_TARGETS[number];
/**
 * Check whether a given output target is a valid one to be set in a Stencil config
 *
 * @param targetType the type which we want to check
 * @returns whether or not the targetType is a valid, configurable output target.
 */
export declare function isValidConfigOutputTarget(targetType: string): targetType is ValidConfigOutputTarget;
export declare const GENERATED_DTS = "components.d.ts";
export {};
