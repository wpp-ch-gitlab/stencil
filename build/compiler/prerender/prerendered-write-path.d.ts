import type * as d from '../../declarations';
export declare const getWriteFilePathFromUrlPath: (manager: d.PrerenderManager, inputHref: string) => string;
