import ts from 'typescript';
import type * as d from '../../declarations';
export declare const updateComponentClass: (transformOpts: d.TransformOptions, classNode: ts.ClassDeclaration, heritageClauses: ts.HeritageClause[] | ts.NodeArray<ts.HeritageClause>, members: ts.ClassElement[]) => ts.VariableStatement | ts.ClassDeclaration;
