import type * as d from '../../declarations';
export declare const hAsync: (nodeName: any, vnodeData: any, ...children: d.ChildType[]) => d.VNode | Promise<d.VNode>;
