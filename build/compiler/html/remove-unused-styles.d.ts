import type * as d from '../../declarations';
export declare const removeUnusedStyles: (doc: Document, diagnostics: d.Diagnostic[]) => void;
