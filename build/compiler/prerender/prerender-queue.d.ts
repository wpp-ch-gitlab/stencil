import type * as d from '../../declarations';
export declare const initializePrerenderEntryUrls: (results: d.PrerenderResults, manager: d.PrerenderManager) => void;
export declare const drainPrerenderQueue: (results: d.PrerenderResults, manager: d.PrerenderManager) => void;
