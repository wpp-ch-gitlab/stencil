export declare const buildId = "__BUILDID__";
export declare const minfyJsId = "__BUILDID:MINIFYJS__";
export declare const optimizeCssId = "__BUILDID:OPTIMIZECSS__";
export declare const parse5Version = "__VERSION:PARSE5__";
export declare const rollupVersion = "__VERSION:ROLLUP__";
export declare const sizzleVersion = "__VERSION:SIZZLE__";
export declare const terserVersion = "__VERSION:TERSER__";
export declare const typescriptVersion = "__VERSION:TYPESCRIPT__";
export declare const vermoji = "__VERMOJI__";
export declare const version = "__VERSION:STENCIL__";
export declare const versions: {
    stencil: string;
    parse5: string;
    rollup: string;
    sizzle: string;
    terser: string;
    typescript: string;
};
