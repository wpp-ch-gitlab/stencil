import { p as promiseResolve, b as bootstrapLazy } from './index-bd1cc1b9.js';

/*
 Stencil Client Patch Esm v2.19.0 | MIT Licensed | https://stenciljs.com
 */
const patchEsm = () => {
    return promiseResolve();
};

const defineCustomElements = (win, options) => {
  if (typeof window === 'undefined') return Promise.resolve();
  return patchEsm().then(() => {
  return bootstrapLazy([["sibling-root",[[6,"sibling-root","sibling-root"]]]], options);
  });
};

export { defineCustomElements };
