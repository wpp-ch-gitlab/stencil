import type * as d from '../../declarations';
export declare const validatePrerender: (config: d.ValidatedConfig, diagnostics: d.Diagnostic[], outputTarget: d.OutputTargetWww) => void;
