import ts from 'typescript';
import type * as d from '../../../declarations';
export declare const componentDecoratorToStatic: (config: d.Config, typeChecker: ts.TypeChecker, diagnostics: d.Diagnostic[], cmpNode: ts.ClassDeclaration, newMembers: ts.ClassElement[], componentDecorator: ts.Decorator) => void;
