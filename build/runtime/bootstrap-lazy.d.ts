import type * as d from '../declarations';
export declare const bootstrapLazy: (lazyBundles: d.LazyBundlesRuntimeData, options?: d.CustomElementsDefineOptions) => void;
