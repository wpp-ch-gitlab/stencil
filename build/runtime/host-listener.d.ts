import type * as d from '../declarations';
export declare const addHostEventListeners: (elm: d.HostElement, hostRef: d.HostRef, listeners: d.ComponentRuntimeHostListener[], attachParentListeners: boolean) => void;
