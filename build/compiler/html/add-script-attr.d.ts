import type * as d from '../../declarations';
export declare const addScriptDataAttribute: (config: d.Config, doc: Document, outputTarget: d.OutputTargetWww) => void;
