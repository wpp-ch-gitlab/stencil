import ts from 'typescript';
import type * as d from '../../declarations';
export declare const addModuleMetadataProxies: (tsSourceFile: ts.SourceFile, moduleFile: d.Module) => ts.SourceFile;
/**
 * Create a call expression for wrapping a component in a proxy. This call expression takes a form:
 * ```ts
 * PROXY_CUSTOM_ELEMENT(ComponentClassName, Metadata);
 * ```
 * where
 * - `PROXY_CUSTOM_ELEMENT` is a Stencil internal identifier that will be replaced with the name of the actual function
 * name at compile name
 * - `ComponentClassName` is the name Stencil component's class
 * - `Metadata` is the compiler metadata associated with the Stencil component
 *
 * @param compilerMeta compiler metadata associated with the component to be wrapped in a proxy
 * @returns the generated call expression
 */
export declare const createComponentMetadataProxy: (compilerMeta: d.ComponentCompilerMeta) => ts.CallExpression;
/**
 * Create a call expression for wrapping a component represented as an anonymous class in a proxy. This call expression
 * takes a form:
 * ```ts
 * PROXY_CUSTOM_ELEMENT(Clazz, Metadata);
 * ```
 * where
 * - `PROXY_CUSTOM_ELEMENT` is a Stencil internal identifier that will be replaced with the name of the actual function
 * name at compile name
 * - `Clazz` is an anonymous class to be proxied
 * - `Metadata` is the compiler metadata associated with the Stencil component
 *
 * @param compilerMeta compiler metadata associated with the component to be wrapped in a proxy
 * @param clazz the anonymous class to proxy
 * @returns the generated call expression
 */
export declare const createAnonymousClassMetadataProxy: (compilerMeta: d.ComponentCompilerMeta, clazz: ts.Expression) => ts.CallExpression;
