import type * as d from '../../declarations';
export declare const createWatchBuild: (config: d.ValidatedConfig, compilerCtx: d.CompilerCtx) => Promise<d.CompilerWatcher>;
