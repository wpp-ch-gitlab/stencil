import type { CompilerSystem } from '../../declarations';
/**
 * Create a node.js-specific {@link CompilerSystem} to be used when Stencil is
 * run from the CLI or via the public API in a node context.
 *
 * This takes an optional param supplying a `process` object to be used.
 *
 * @param c an optional object wrapping a `process` object
 * @returns a node.js `CompilerSystem` object
 */
export declare function createNodeSys(c?: {
    process?: any;
}): CompilerSystem;
