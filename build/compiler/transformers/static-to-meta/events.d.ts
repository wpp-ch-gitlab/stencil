import ts from 'typescript';
import type * as d from '../../../declarations';
export declare const parseStaticEvents: (staticMembers: ts.ClassElement[]) => d.ComponentCompilerEvent[];
