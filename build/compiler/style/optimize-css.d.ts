import type * as d from '../../declarations';
export declare const optimizeCss: (config: d.Config, compilerCtx: d.CompilerCtx, diagnostics: d.Diagnostic[], styleText: string, filePath: string) => Promise<string>;
