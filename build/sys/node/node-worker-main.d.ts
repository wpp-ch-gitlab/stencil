/// <reference types="node" />
/// <reference types="node" />
import * as cp from 'child_process';
import { EventEmitter } from 'events';
import type * as d from '../../declarations';
export declare class NodeWorkerMain extends EventEmitter {
    id: number;
    childProcess: cp.ChildProcess;
    tasks: Map<number, d.CompilerWorkerTask>;
    exitCode: number;
    processQueue: boolean;
    sendQueue: d.MsgToWorker[];
    stopped: boolean;
    successfulMessage: boolean;
    totalTasksAssigned: number;
    constructor(id: number, forkModulePath: string);
    fork(forkModulePath: string): void;
    run(task: d.CompilerWorkerTask): void;
    sendToWorker(msg: d.MsgToWorker): void;
    receiveFromWorker(msgFromWorker: d.MsgFromWorker): void;
    stop(): void;
}
