export class MockStorage {
    constructor() {
        this.items = new Map();
    }
    key(_value) {
        //
    }
    getItem(key) {
        key = String(key);
        if (this.items.has(key)) {
            return this.items.get(key);
        }
        return null;
    }
    setItem(key, value) {
        if (value == null) {
            value = 'null';
        }
        this.items.set(String(key), String(value));
    }
    removeItem(key) {
        this.items.delete(String(key));
    }
    clear() {
        this.items.clear();
    }
}
//# sourceMappingURL=storage.js.map