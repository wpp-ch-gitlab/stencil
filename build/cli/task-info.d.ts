import type { CompilerSystem, Logger } from '../declarations';
import type { CoreCompiler } from './load-compiler';
export declare const taskInfo: (coreCompiler: CoreCompiler, sys: CompilerSystem, logger: Logger) => void;
