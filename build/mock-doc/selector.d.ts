import { MockElement } from './node';
export declare function matches(selector: string, elm: MockElement): boolean;
export declare function selectOne(selector: string, elm: MockElement): Element;
export declare function selectAll(selector: string, elm: MockElement): Element[];
