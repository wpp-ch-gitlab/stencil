import type * as d from '../../../declarations';
/**
 * Creates an instance of a logger
 * @returns the new logger instance
 */
export declare const createLogger: () => d.Logger;
