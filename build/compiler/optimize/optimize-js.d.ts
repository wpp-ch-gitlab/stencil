import { OptimizeJsInput, OptimizeJsOutput } from '../../declarations';
export declare const optimizeJs: (inputOpts: OptimizeJsInput) => Promise<OptimizeJsOutput>;
