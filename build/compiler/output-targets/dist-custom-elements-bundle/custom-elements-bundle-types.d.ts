import type * as d from '../../../declarations';
export declare const generateCustomElementsBundleTypes: (config: d.ValidatedConfig, compilerCtx: d.CompilerCtx, buildCtx: d.BuildCtx, distDtsFilePath: string) => Promise<void>;
