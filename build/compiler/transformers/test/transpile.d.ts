import type * as d from '@stencil/core/declarations';
import ts from 'typescript';
/**
 * Testing utility for transpiling provided string containing valid Stencil code
 *
 * @param input the code to transpile
 * @param config a Stencil configuration to apply during the transpilation
 * @param compilerCtx a compiler context to use in the transpilation process
 * @param beforeTransformers TypeScript transformers that should be applied before the code is emitted
 * @param afterTransformers TypeScript transformers that should be applied after the code is emitted
 * @returns the result of the transpilation step
 */
export declare function transpileModule(input: string, config?: d.Config | null, compilerCtx?: d.CompilerCtx | null, beforeTransformers?: ts.TransformerFactory<ts.SourceFile>[], afterTransformers?: ts.TransformerFactory<ts.SourceFile>[]): {
    outputText: string;
    compilerCtx: d.CompilerCtx;
    buildCtx: d.BuildCtx;
    diagnostics: d.Diagnostic[];
    moduleFile: d.Module;
    cmps: d.ComponentCompilerMeta[];
    cmp: d.ComponentCompilerMeta;
    componentClassName: string;
    tagName: string;
    properties: d.ComponentCompilerProperty[];
    virtualProperties: d.ComponentCompilerVirtualProperty[];
    property: d.ComponentCompilerProperty;
    states: d.ComponentCompilerState[];
    state: d.ComponentCompilerState;
    listeners: d.ComponentCompilerListener[];
    listener: d.ComponentCompilerListener;
    events: d.ComponentCompilerEvent[];
    event: d.ComponentCompilerEvent;
    methods: d.ComponentCompilerMethod[];
    method: d.ComponentCompilerMethod;
    elementRef: string;
    legacyContext: d.ComponentCompilerLegacyContext[];
    legacyConnect: d.ComponentCompilerLegacyConnect[];
};
export declare function getStaticGetter(output: string, prop: string): any;
