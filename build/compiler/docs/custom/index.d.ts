import type * as d from '../../../declarations';
export declare const generateCustomDocs: (config: d.ValidatedConfig, docsData: d.JsonDocs, outputTargets: d.OutputTarget[]) => Promise<void>;
