import { TranspileOptions, TranspileResults } from '../declarations';
export declare const transpile: (code: string, opts?: TranspileOptions) => Promise<TranspileResults>;
export declare const transpileSync: (code: string, opts?: TranspileOptions) => TranspileResults;
