import type * as d from '../../declarations';
export declare const validateServiceWorker: (config: d.ValidatedConfig, outputTarget: d.OutputTargetWww) => void;
