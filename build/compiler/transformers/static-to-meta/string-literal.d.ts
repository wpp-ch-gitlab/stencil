import ts from 'typescript';
import type * as d from '../../../declarations';
export declare const parseStringLiteral: (m: d.Module | d.ComponentCompilerMeta, node: ts.StringLiteral) => void;
