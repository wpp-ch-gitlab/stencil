import ts from 'typescript';
import type * as d from '../../../declarations';
export declare const parseClassMethods: (cmpNode: ts.ClassDeclaration, cmpMeta: d.ComponentCompilerMeta) => void;
