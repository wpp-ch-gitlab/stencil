import type * as d from '../../../declarations';
export declare const validateCustomOutput: (config: d.ValidatedConfig, diagnostics: d.Diagnostic[], userOutputs: d.OutputTarget[]) => d.OutputTargetCustom[];
