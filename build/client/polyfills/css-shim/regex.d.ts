export declare const VAR_USAGE_START: RegExp;
export declare const VAR_ASSIGN_START: RegExp;
export declare const COMMENTS: RegExp;
export declare const TRAILING_LINES: RegExp;
