import { CssNode, SerializeCssOptions } from './css-parse-declarations';
export declare const serializeCss: (stylesheet: CssNode, serializeOpts: SerializeCssOptions) => string;
