import type * as d from '../declarations';
export declare const initializeComponent: (elm: d.HostElement, hostRef: d.HostRef, cmpMeta: d.ComponentRuntimeMeta, hmrVersionId?: string, Cstr?: any) => Promise<void>;
export declare const fireConnectedCallback: (instance: any) => void;
