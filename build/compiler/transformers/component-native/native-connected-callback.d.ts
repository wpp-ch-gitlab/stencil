import ts from 'typescript';
import type * as d from '../../../declarations';
export declare const addNativeConnectedCallback: (classMembers: ts.ClassElement[], cmp: d.ComponentCompilerMeta) => void;
