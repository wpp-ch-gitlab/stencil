import type * as d from '../declarations';
export declare const patchCloneNode: (HostElementPrototype: any) => void;
export declare const patchSlotAppendChild: (HostElementPrototype: any) => void;
/**
 * Patches the text content of an unnamed slotted node inside a scoped component
 * @param hostElementPrototype the `Element` to be patched
 * @param cmpMeta component runtime metadata used to determine if the component should be patched or not
 */
export declare const patchTextContent: (hostElementPrototype: HTMLElement, cmpMeta: d.ComponentRuntimeMeta) => void;
export declare const patchChildSlotNodes: (elm: any, cmpMeta: d.ComponentRuntimeMeta) => void;
