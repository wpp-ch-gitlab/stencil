/// <reference types="node" />
export declare const IS_NODE_ENV: boolean;
export declare const OS_PLATFORM: string;
export declare const IS_WINDOWS_ENV: boolean;
export declare const IS_CASE_SENSITIVE_FILE_NAMES: boolean;
export declare const IS_BROWSER_ENV: boolean;
export declare const IS_WEB_WORKER_ENV: boolean;
export declare const HAS_WEB_WORKER: boolean;
export declare const IS_FETCH_ENV: boolean;
export declare const requireFunc: NodeRequire | (() => void);
export declare const getCurrentDirectory: () => string;
