import type * as d from '../../declarations';
export declare const outputAngular: (config: d.ValidatedConfig, compilerCtx: d.CompilerCtx, buildCtx: d.BuildCtx) => Promise<void>;
export declare const angularDirectiveProxyOutput: (config: d.ValidatedConfig, compilerCtx: d.CompilerCtx, buildCtx: d.BuildCtx, outputTarget: d.OutputTargetAngular) => Promise<[import("..").FsWriteResults, any, void]>;
export declare const GENERATED_DTS = "components.d.ts";
