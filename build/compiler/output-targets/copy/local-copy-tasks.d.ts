import type * as d from '../../../declarations';
export declare const getSrcAbsPath: (config: d.ValidatedConfig, src: string) => string;
export declare const getDestAbsPath: (src: string, destAbsPath: string, destRelPath: string) => string;
