import { SyncOpts } from 'resolve';
import type * as d from '../../../declarations';
import { InMemoryFileSystem } from '../in-memory-fs';
export declare const resolveRemoteModuleIdSync: (config: d.Config, inMemoryFs: InMemoryFileSystem, opts: d.ResolveModuleIdOptions) => {
    resolvedUrl: string;
    packageJson: d.PackageJsonData;
};
export declare const resolveModuleIdSync: (sys: d.CompilerSystem, inMemoryFs: InMemoryFileSystem, opts: d.ResolveModuleIdOptions) => string;
export declare const createCustomResolverSync: (sys: d.CompilerSystem, inMemoryFs: InMemoryFileSystem, exts: string[]) => SyncOpts;
