import ts from 'typescript';
import type * as d from '../../../declarations';
export declare const watchDecoratorsToStatic: (config: d.Config, diagnostics: d.Diagnostic[], decoratedProps: ts.ClassElement[], watchable: Set<string>, newMembers: ts.ClassElement[]) => void;
