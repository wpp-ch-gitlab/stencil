import ts from 'typescript';
import type * as d from '../../../declarations';
export declare const convertDecoratorsToStatic: (config: d.Config, diagnostics: d.Diagnostic[], typeChecker: ts.TypeChecker) => ts.TransformerFactory<ts.SourceFile>;
/**
 * Helper util for updating the constructor on a class declaration AST node.
 *
 * @param classNode the class node whose constructor will be updated
 * @param classMembers a list of class members for that class
 * @param statements a list of statements which should be added to the
 * constructor
 * @returns a list of updated class elements
 */
export declare const updateConstructor: (classNode: ts.ClassDeclaration, classMembers: ts.ClassElement[], statements: ts.Statement[]) => ts.ClassElement[];
