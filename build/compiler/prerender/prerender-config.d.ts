import type * as d from '../../declarations';
export declare const getPrerenderConfig: (diagnostics: d.Diagnostic[], prerenderConfigPath: string) => d.PrerenderConfig;
