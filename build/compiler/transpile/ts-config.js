import ts from 'typescript';
import { isOutputTargetDistTypes } from '../output-targets/output-utils';
export const getTsOptionsToExtend = (config) => {
    const tsOptions = {
        experimentalDecorators: true,
        declaration: config.outputTargets.some(isOutputTargetDistTypes),
        module: ts.ModuleKind.ESNext,
        moduleResolution: ts.ModuleResolutionKind.NodeJs,
        noEmitOnError: false,
        outDir: config.cacheDir || config.sys.tmpDirSync(),
        sourceMap: config.sourceMap,
        inlineSources: config.sourceMap,
    };
    return tsOptions;
};
//# sourceMappingURL=ts-config.js.map