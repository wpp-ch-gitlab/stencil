import { ParseCssResults } from './css-parse-declarations';
export declare const parseCss: (css: string, filePath?: string) => ParseCssResults;
