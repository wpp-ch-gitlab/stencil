/// <reference types="node" />
/// <reference types="node" />
import { EventEmitter } from 'events';
import type * as d from '../../declarations';
import { NodeWorkerMain } from './node-worker-main';
export declare class NodeWorkerController extends EventEmitter implements d.WorkerMainController {
    forkModulePath: string;
    workerIds: number;
    stencilId: number;
    isEnding: boolean;
    taskQueue: d.CompilerWorkerTask[];
    workers: NodeWorkerMain[];
    maxWorkers: number;
    useForkedWorkers: boolean;
    mainThreadRunner: {
        [fnName: string]: (...args: any[]) => Promise<any>;
    };
    constructor(forkModulePath: string, maxConcurrentWorkers: number);
    onError(err: NodeJS.ErrnoException, workerId: number): void;
    onExit(workerId: number): void;
    startWorkers(): void;
    startWorker(): void;
    stopWorker(workerId: number): void;
    processTaskQueue(): void;
    send(...args: any[]): any;
    handler(name: string): (...args: any[]) => any;
    cancelTasks(): void;
    destroy(): void;
}
export declare function getNextWorker(workers: NodeWorkerMain[]): NodeWorkerMain;
