import '@stencil/core/compiler';
import * as nodeApi from '@sys-api-node';
import { initNodeWorkerThread } from './node-worker-thread';
const coreCompiler = global.stencil;
const nodeSys = nodeApi.createNodeSys({ process: process });
const msgHandler = coreCompiler.createWorkerMessageHandler(nodeSys);
initNodeWorkerThread(process, msgHandler);
//# sourceMappingURL=worker.js.map