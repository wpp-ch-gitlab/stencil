import ts from 'typescript';
import * as d from '@stencil/core/declarations';
export declare const addTagNamePostfix: (tagName: string, postfix?: string) => string;
export declare const isTargetWebComponent: (tagName: string) => boolean;
export declare const convertTagNames: (config: d.Config) => ts.TransformerFactory<ts.SourceFile>;
export declare const updateCallExpression: (config: d.Config, node: ts.CallExpression) => boolean;
