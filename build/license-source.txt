@rollup/plugin-commonjs
  @rollup/pluginutils
  commondir
  estree-walker
  glob
  is-reference
  magic-string
  resolve

@rollup/plugin-json
  @rollup/pluginutils

@rollup/plugin-node-resolve
  @rollup/pluginutils
  @types/resolve
  builtin-modules
  deepmerge
  is-module
  resolve

@rollup/pluginutils
  @types/estree
  estree-walker
  picomatch

@yarnpkg/lockfile

ansi-colors

autoprefixer
  browserslist
  caniuse-lite
  fraction.js
  normalize-range
  picocolors
  postcss-value-parser

balanced-match

brace-expansion
  balanced-match

browserslist
  caniuse-lite
  electron-to-chromium
  node-releases
  update-browserslist-db

buffer-from

builtin-modules

caniuse-lite

commondir

deepmerge

define-lazy-prop

electron-to-chromium

entities

escalade

estree-walker

exit

fraction.js

fs.realpath

function-bind

glob
  fs.realpath
  inflight
  inherits
  minimatch
  once

graceful-fs

has
  function-bind

inflight
  once
  wrappy

inherits

is-core-module
  has

is-docker

is-extglob

is-module

is-reference
  @types/estree

is-wsl
  is-docker

kleur

lodash

lru-cache
  yallist

magic-string
  sourcemap-codec

merge-source-map
  source-map

minimatch
  brace-expansion

nanoid

node-fetch
  whatwg-url

node-releases

normalize-range

once
  wrappy

open
  define-lazy-prop
  is-docker
  is-wsl

parse5
  entities

path-parse

picocolors

picomatch

pixelmatch
  pngjs

pngjs

postcss
  nanoid
  picocolors
  source-map-js

postcss-value-parser

prompts
  kleur
  sisteransi

punycode

resolve
  is-core-module
  path-parse
  supports-preserve-symlinks-flag

rollup

semver
  lru-cache

sisteransi

sizzle

source-map

source-map-js

source-map-support
  buffer-from
  source-map

sourcemap-codec

supports-preserve-symlinks-flag

terser
  commander
  source-map
  source-map-support

tr46
  punycode

typescript

update-browserslist-db
  escalade
  picocolors

webidl-conversions

whatwg-url
  lodash
  tr46
  webidl-conversions

wrappy

ws

yallist
