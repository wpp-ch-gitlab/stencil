import ts from 'typescript';
import { addCoreRuntimeApi, CREATE_EVENT, RUNTIME_APIS } from './core-runtime-apis';
export const addCreateEvents = (moduleFile, cmp) => {
    return cmp.events.map((ev) => {
        addCoreRuntimeApi(moduleFile, RUNTIME_APIS.createEvent);
        return ts.factory.createExpressionStatement(ts.factory.createAssignment(ts.factory.createPropertyAccessExpression(ts.factory.createThis(), ts.factory.createIdentifier(ev.method)), ts.factory.createCallExpression(ts.factory.createIdentifier(CREATE_EVENT), undefined, [
            ts.factory.createThis(),
            ts.factory.createStringLiteral(ev.name),
            ts.factory.createNumericLiteral(computeFlags(ev)),
        ])));
    });
};
const computeFlags = (eventMeta) => {
    let flags = 0;
    if (eventMeta.bubbles) {
        flags |= 4 /* EVENT_FLAGS.Bubbles */;
    }
    if (eventMeta.composed) {
        flags |= 2 /* EVENT_FLAGS.Composed */;
    }
    if (eventMeta.cancelable) {
        flags |= 1 /* EVENT_FLAGS.Cancellable */;
    }
    return flags;
};
//# sourceMappingURL=create-event.js.map