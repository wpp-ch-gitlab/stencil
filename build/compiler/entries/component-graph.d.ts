import type * as d from '../../declarations';
export declare const generateModuleGraph: (cmps: d.ComponentCompilerMeta[], bundleModules: ReadonlyArray<d.BundleModule>) => Map<string, string[]>;
