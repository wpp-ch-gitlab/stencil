import ts from 'typescript';
import type * as d from '../../../declarations';
export declare const parseStaticMethods: (staticMembers: ts.ClassElement[]) => d.ComponentCompilerMethod[];
