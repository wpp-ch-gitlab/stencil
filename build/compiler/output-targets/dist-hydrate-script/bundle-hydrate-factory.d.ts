import type * as d from '../../../declarations';
export declare const bundleHydrateFactory: (config: d.ValidatedConfig, compilerCtx: d.CompilerCtx, buildCtx: d.BuildCtx, _build: d.BuildConditionals, appFactoryEntryCode: string) => Promise<import("rollup").RollupBuild>;
