import ts from 'typescript';
import type * as d from '../../declarations';
export declare const addCreateEvents: (moduleFile: d.Module, cmp: d.ComponentCompilerMeta) => ts.ExpressionStatement[];
