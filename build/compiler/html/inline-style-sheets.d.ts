import type * as d from '../../declarations';
export declare const inlineStyleSheets: (compilerCtx: d.CompilerCtx, doc: Document, maxSize: number, outputTarget: d.OutputTargetWww) => Promise<void[]>;
