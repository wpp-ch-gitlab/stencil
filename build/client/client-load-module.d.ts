import type * as d from '../declarations';
export declare const cmpModules: Map<string, {
    [exportName: string]: d.ComponentConstructor;
}>;
export declare const loadModule: (cmpMeta: Omit<d.ComponentRuntimeMeta, '$registryTagName$'>, hostRef: d.HostRef, hmrVersionId?: string) => Promise<d.ComponentConstructor> | d.ComponentConstructor;
