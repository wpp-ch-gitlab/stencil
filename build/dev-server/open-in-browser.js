import open from 'open';
export async function openInBrowser(opts) {
    // await open(opts.url, { app: ['google chrome', '--auto-open-devtools-for-tabs'] });
    await open(opts.url);
}
//# sourceMappingURL=open-in-browser.js.map