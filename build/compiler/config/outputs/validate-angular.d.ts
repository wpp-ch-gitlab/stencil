import type * as d from '../../../declarations';
import { OutputTargetAngular } from '../../../declarations';
export declare const validateAngular: (config: d.ValidatedConfig, userOutputs: d.OutputTarget[]) => OutputTargetAngular[];
