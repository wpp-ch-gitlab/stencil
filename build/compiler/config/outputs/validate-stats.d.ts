import type * as d from '../../../declarations';
export declare const validateStats: (userConfig: d.ValidatedConfig, userOutputs: d.OutputTarget[]) => d.OutputTargetStats[];
