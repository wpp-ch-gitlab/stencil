import { normalizePath } from '@utils';
import pathBrowserify from 'path-browserify';
import { IS_NODE_ENV, requireFunc } from '../environment';
export let basename;
export let dirname;
export let extname;
export let isAbsolute;
export let join;
export let normalize;
export let parse;
export let relative;
export let resolve;
export let sep;
export let delimiter;
export let posix;
export let win32;
export const path = {};
export const setPlatformPath = (platformPath) => {
    if (!platformPath) {
        platformPath = pathBrowserify;
    }
    Object.assign(path, platformPath);
    const normalizeOrg = path.normalize;
    const joinOrg = path.join;
    const relativeOrg = path.relative;
    const resolveOrg = path.resolve;
    normalize = path.normalize = (...args) => normalizePath(normalizeOrg.apply(path, args));
    join = path.join = (...args) => normalizePath(joinOrg.apply(path, args));
    relative = path.relative = (...args) => normalizePath(relativeOrg.apply(path, args));
    resolve = path.resolve = (...args) => normalizePath(resolveOrg.apply(path, args));
    basename = path.basename;
    dirname = path.dirname;
    extname = path.extname;
    isAbsolute = path.isAbsolute;
    parse = path.parse;
    sep = path.sep;
    delimiter = path.delimiter;
    posix = path.posix;
    if (path.win32) {
        win32 = path.win32;
    }
    else {
        win32 = { ...posix };
        win32.sep = '\\';
    }
};
setPlatformPath(IS_NODE_ENV ? requireFunc('path') : pathBrowserify);
export default path;
//# sourceMappingURL=path.js.map