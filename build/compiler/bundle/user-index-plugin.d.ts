import type { Plugin } from 'rollup';
import type * as d from '../../declarations';
export declare const userIndexPlugin: (config: d.Config, compilerCtx: d.CompilerCtx) => Plugin;
