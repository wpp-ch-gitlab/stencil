export declare function openInBrowser(opts: {
    url: string;
}): Promise<void>;
