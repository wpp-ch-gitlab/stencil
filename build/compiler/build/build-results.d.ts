import type * as d from '../../declarations';
export declare const generateBuildResults: (config: d.Config, compilerCtx: d.CompilerCtx, buildCtx: d.BuildCtx) => d.CompilerBuildResults;
