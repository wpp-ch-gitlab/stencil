import { optimizeCss } from '../optimize/optimize-css';
import { prepareModule } from '../optimize/optimize-module';
import { prerenderWorker } from '../prerender/prerender-worker';
import { transformCssToEsm } from '../style/css-to-esm';
export const createWorkerContext = (sys) => ({
    transformCssToEsm,
    prepareModule,
    optimizeCss,
    prerenderWorker: (prerenderRequest) => prerenderWorker(sys, prerenderRequest),
});
export const createWorkerMessageHandler = (sys) => {
    const workerCtx = createWorkerContext(sys);
    return (msgToWorker) => {
        const fnName = msgToWorker.args[0];
        const fnArgs = msgToWorker.args.slice(1);
        const fn = workerCtx[fnName];
        if (typeof fn === 'function') {
            return fn(...fnArgs);
        }
    };
};
//# sourceMappingURL=worker-thread.js.map