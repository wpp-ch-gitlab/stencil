import type { Plugin } from 'rollup';
import type * as d from '../../declarations';
export declare const serverPlugin: (config: d.ValidatedConfig, platform: string) => Plugin;
