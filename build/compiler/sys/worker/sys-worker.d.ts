import type { ValidatedConfig } from '../../../declarations';
export declare const createSysWorker: (config: ValidatedConfig) => import("../../../declarations").CompilerWorkerContext;
