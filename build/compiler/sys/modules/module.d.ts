/**
 * Node builtin modules as of v14.5.0
 */
export declare const NODE_BUILTINS: string[];
export default class Module {
    static get builtinModules(): string[];
}
