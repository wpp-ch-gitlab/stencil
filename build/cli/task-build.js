import { printCheckVersionResults, startCheckVersion } from './check-version';
import { startupCompilerLog } from './logs';
import { runPrerenderTask } from './task-prerender';
import { taskWatch } from './task-watch';
import { telemetryBuildFinishedAction } from './telemetry/telemetry';
export const taskBuild = async (coreCompiler, config) => {
    var _a;
    if (config.flags.watch) {
        // watch build
        await taskWatch(coreCompiler, config);
        return;
    }
    // one-time build
    let exitCode = 0;
    try {
        config.logger.warn('==> 32 - Patched Stencil with tagNamePostfix support');
        if ((_a = config.flags) === null || _a === void 0 ? void 0 : _a.tagNamePostfix) {
            config.logger.debug('==> Version Postfix:', config.flags.tagNamePostfix);
        }
        startupCompilerLog(coreCompiler, config);
        const versionChecker = startCheckVersion(config, coreCompiler.version);
        const compiler = await coreCompiler.createCompiler(config);
        const results = await compiler.build();
        await telemetryBuildFinishedAction(config.sys, config, coreCompiler, results);
        await compiler.destroy();
        if (results.hasError) {
            exitCode = 1;
        }
        else if (config.flags.prerender) {
            const prerenderDiagnostics = await runPrerenderTask(coreCompiler, config, results.hydrateAppFilePath, results.componentGraph, null);
            config.logger.printDiagnostics(prerenderDiagnostics);
            if (prerenderDiagnostics.some((d) => d.level === 'error')) {
                exitCode = 1;
            }
        }
        await printCheckVersionResults(versionChecker);
    }
    catch (e) {
        exitCode = 1;
        config.logger.error(e);
    }
    if (exitCode > 0) {
        return config.sys.exit(exitCode);
    }
};
//# sourceMappingURL=task-build.js.map