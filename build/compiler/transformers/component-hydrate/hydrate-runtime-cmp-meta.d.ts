import ts from 'typescript';
import type * as d from '../../../declarations';
export declare const addHydrateRuntimeCmpMeta: (classMembers: ts.ClassElement[], cmp: d.ComponentCompilerMeta) => void;
