import type * as d from '../../declarations';
export declare const initAppUpdate: (win: d.DevClientWindow, config: d.DevClientConfig) => void;
export declare const appReset: (win: d.DevClientWindow, config: d.DevClientConfig, cb: () => void) => void;
