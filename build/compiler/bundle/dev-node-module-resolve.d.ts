import { PartialResolvedId } from 'rollup';
import type * as d from '../../declarations';
import { InMemoryFileSystem } from '../sys/in-memory-fs';
export declare const devNodeModuleResolveId: (config: d.Config, inMemoryFs: InMemoryFileSystem, resolvedId: PartialResolvedId, importee: string) => Promise<PartialResolvedId>;
