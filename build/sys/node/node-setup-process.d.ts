import type { Logger } from '../../declarations';
export declare function setupNodeProcess(c: {
    process: any;
    logger: Logger;
}): void;
