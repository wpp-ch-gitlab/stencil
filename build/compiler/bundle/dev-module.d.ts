import type * as d from '../../declarations';
export declare const compilerRequest: (config: d.ValidatedConfig, compilerCtx: d.CompilerCtx, data: d.CompilerRequest) => Promise<d.CompilerRequestResponse>;
