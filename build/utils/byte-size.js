/**
 * Used to learn the size of a string in bytes.
 *
 * @param str The string to measure
 * @returns number
 */
export const byteSize = (str) => Buffer.byteLength(str, 'utf8');
//# sourceMappingURL=byte-size.js.map