import type { Plugin } from 'rollup';
import type * as d from '../../declarations';
export declare const workerPlugin: (config: d.ValidatedConfig, compilerCtx: d.CompilerCtx, buildCtx: d.BuildCtx, platform: string, inlineWorkers: boolean) => Plugin;
export declare const WORKER_HELPERS: string;
