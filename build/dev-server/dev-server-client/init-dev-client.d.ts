import type * as d from '../../declarations';
export declare const initDevClient: (win: d.DevClientWindow, config: d.DevClientConfig) => void;
