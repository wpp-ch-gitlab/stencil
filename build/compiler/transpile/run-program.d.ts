import type ts from 'typescript';
import type * as d from '../../declarations';
export declare const runTsProgram: (config: d.ValidatedConfig, compilerCtx: d.CompilerCtx, buildCtx: d.BuildCtx, tsBuilder: ts.BuilderProgram) => Promise<boolean>;
