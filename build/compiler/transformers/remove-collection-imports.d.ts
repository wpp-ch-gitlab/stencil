import ts from 'typescript';
import type * as d from '../../declarations';
export declare const removeCollectionImports: (compilerCtx: d.CompilerCtx) => ts.TransformerFactory<ts.SourceFile>;
