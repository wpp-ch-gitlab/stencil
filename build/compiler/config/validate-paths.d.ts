import type * as d from '../../declarations';
/**
 * Do logical-level validation (as opposed to type-level validation)
 * for various properties in the user-supplied config which represent
 * filesystem paths.
 *
 * @param config a validated user-supplied configuration
 */
export declare const validatePaths: (config: d.ValidatedConfig) => void;
