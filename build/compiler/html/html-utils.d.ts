import type * as d from '../../declarations';
export declare const getAbsoluteBuildDir: (outputTarget: d.OutputTargetWww) => string;
