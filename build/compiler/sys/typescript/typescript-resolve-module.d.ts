import ts from 'typescript';
import type * as d from '../../../declarations';
import { InMemoryFileSystem } from '../in-memory-fs';
export declare const patchTypeScriptResolveModule: (config: d.Config, inMemoryFs: InMemoryFileSystem) => void;
export declare const tsResolveModuleName: (config: d.Config, compilerCtx: d.CompilerCtx, moduleName: string, containingFile: string) => ts.ResolvedModuleWithFailedLookupLocations;
export declare const tsResolveModuleNamePackageJsonPath: (config: d.Config, compilerCtx: d.CompilerCtx, moduleName: string, containingFile: string) => string;
export declare const patchedTsResolveModule: (config: d.Config, inMemoryFs: InMemoryFileSystem, moduleName: string, containingFile: string) => ts.ResolvedModuleWithFailedLookupLocations;
export declare const tsResolveNodeModule: (config: d.Config, inMemoryFs: InMemoryFileSystem, moduleId: string, containingFile: string) => ts.ResolvedModuleWithFailedLookupLocations;
export declare const ensureExtension: (fileName: string, containingFile: string) => string;
