import ts from 'typescript';
import type * as d from '../../../declarations';
export declare const updateNativeConstructor: (classMembers: ts.ClassElement[], moduleFile: d.Module, cmp: d.ComponentCompilerMeta, ensureSuper: boolean) => void;
