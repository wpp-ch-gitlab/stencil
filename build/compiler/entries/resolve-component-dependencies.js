import { flatOne, unique } from '@utils';
export function resolveComponentDependencies(cmps) {
    computeDependencies(cmps);
    computeDependents(cmps);
}
function computeDependencies(cmps) {
    const visited = new Set();
    cmps.forEach((cmp) => {
        resolveTransitiveDependencies(cmp, cmps, visited);
        cmp.dependencies = unique(cmp.dependencies).sort();
    });
}
function computeDependents(cmps) {
    cmps.forEach((cmp) => {
        resolveTransitiveDependents(cmp, cmps);
    });
}
function resolveTransitiveDependencies(cmp, cmps, visited) {
    if (visited.has(cmp)) {
        return cmp.dependencies;
    }
    visited.add(cmp);
    const dependencies = unique(cmp.potentialCmpRefs
        .filter((registryTagName) => cmps.some((c) => c.registryTagName === registryTagName)));
    const dependenciesCmps = dependencies
        .map((registryTagName) => cmps.find((c) => c.registryTagName === registryTagName));
    cmp.dependencies = cmp.directDependencies = dependenciesCmps.map((c) => c.tagName);
    const transitiveDeps = flatOne(dependenciesCmps.map((c) => resolveTransitiveDependencies(c, cmps, visited)));
    return (cmp.dependencies = [...cmp.directDependencies, ...transitiveDeps]);
}
function resolveTransitiveDependents(cmp, cmps) {
    cmp.dependents = cmps
        .filter((c) => c.dependencies.includes(cmp.tagName))
        .map((c) => c.tagName)
        .sort();
    cmp.directDependents = cmps
        .filter((c) => c.directDependencies.includes(cmp.tagName))
        .map((c) => c.tagName)
        .sort();
}
//# sourceMappingURL=resolve-component-dependencies.js.map