import type * as d from '../../declarations';
export declare const validatePlugins: (config: d.UnvalidatedConfig, diagnostics: d.Diagnostic[]) => void;
