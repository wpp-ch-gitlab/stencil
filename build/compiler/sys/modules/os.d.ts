/// <reference types="node" />
export declare const EOL = "\n";
export declare const platform: () => "" | NodeJS.Platform;
declare const _default: {
    EOL: string;
    platform: () => "" | NodeJS.Platform;
};
export default _default;
