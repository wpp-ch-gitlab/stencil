import ts from 'typescript';
import type * as d from '../../../declarations';
export declare const addNativeStaticStyle: (classMembers: ts.ClassElement[], cmp: d.ComponentCompilerMeta) => void;
