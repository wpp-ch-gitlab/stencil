import type * as d from '../../../declarations';
export declare const generateLazyModules: (config: d.ValidatedConfig, compilerCtx: d.CompilerCtx, buildCtx: d.BuildCtx, outputTargetType: string, destinations: string[], results: d.RollupResult[], sourceTarget: d.SourceTarget, isBrowserBuild: boolean, sufix: string) => Promise<d.BundleModule[]>;
export declare const sortBundleModules: (a: d.BundleModule, b: d.BundleModule) => -1 | 1 | 0;
export declare const sortBundleComponents: (a: d.ComponentCompilerMeta, b: d.ComponentCompilerMeta) => -1 | 1 | 0;
