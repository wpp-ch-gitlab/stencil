import { createConfigFlags } from '../../cli/config-flags';
import { setPlatformPath } from '../sys/modules/path';
import { createLogger } from './logger/console-logger';
import { createSystem } from './stencil-sys';
export const getConfig = (userConfig) => {
    var _a, _b, _c, _d, _e;
    const logger = (_a = userConfig.logger) !== null && _a !== void 0 ? _a : createLogger();
    const config = {
        ...userConfig,
        flags: createConfigFlags((_b = userConfig.flags) !== null && _b !== void 0 ? _b : {}),
        logger,
        outputTargets: (_c = userConfig.outputTargets) !== null && _c !== void 0 ? _c : [],
        rootDir: (_d = userConfig.rootDir) !== null && _d !== void 0 ? _d : '/',
        sys: (_e = userConfig.sys) !== null && _e !== void 0 ? _e : createSystem({ logger }),
        testing: userConfig !== null && userConfig !== void 0 ? userConfig : {},
    };
    setPlatformPath(config.sys.platformPath);
    if (config.flags.debug || config.flags.verbose) {
        config.logLevel = 'debug';
    }
    else if (config.flags.logLevel) {
        config.logLevel = config.flags.logLevel;
    }
    else if (typeof config.logLevel !== 'string') {
        config.logLevel = 'info';
    }
    config.logger.setLevel(config.logLevel);
    return config;
};
//# sourceMappingURL=config.js.map