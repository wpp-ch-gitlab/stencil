import { mockCompilerSystem, mockLoadConfigInit, mockLogger } from '@stencil/core/testing';
import path from 'path';
import { validateConfig } from '../validate-config';
describe('validatePaths', () => {
    let userConfig;
    const logger = mockLogger();
    const sys = mockCompilerSystem();
    const ROOT = path.resolve('/');
    beforeEach(() => {
        userConfig = {
            sys: sys,
            logger: logger,
            rootDir: path.join(ROOT, 'User', 'my-app'),
            namespace: 'Testing',
        };
    });
    it('should set absolute cacheDir', () => {
        userConfig.cacheDir = path.join(ROOT, 'some', 'custom', 'cache');
        const { config } = validateConfig(userConfig, mockLoadConfigInit());
        expect(config.cacheDir).toBe(path.join(ROOT, 'some', 'custom', 'cache'));
    });
    it('should set relative cacheDir', () => {
        userConfig.cacheDir = 'custom-cache';
        const { config } = validateConfig(userConfig, mockLoadConfigInit());
        expect(config.cacheDir).toBe(path.join(ROOT, 'User', 'my-app', 'custom-cache'));
    });
    it('should set default cacheDir', () => {
        const { config } = validateConfig(userConfig, mockLoadConfigInit());
        expect(config.cacheDir).toBe(path.join(ROOT, 'User', 'my-app', '.stencil'));
    });
    it('should set default wwwIndexHtml and convert to absolute path', () => {
        const { config } = validateConfig(userConfig, mockLoadConfigInit());
        expect(path.basename(config.outputTargets[0].indexHtml)).toBe('index.html');
        expect(path.isAbsolute(config.outputTargets[0].indexHtml)).toBe(true);
    });
    it('should convert a custom wwwIndexHtml to absolute path', () => {
        userConfig.outputTargets = [
            {
                type: 'www',
                indexHtml: path.join('assets', 'custom-index.html'),
            },
        ];
        const { config } = validateConfig(userConfig, mockLoadConfigInit());
        expect(path.basename(config.outputTargets[0].indexHtml)).toBe('custom-index.html');
        expect(path.isAbsolute(config.outputTargets[0].indexHtml)).toBe(true);
    });
    it('should set default indexHtmlSrc and convert to absolute path', () => {
        const { config } = validateConfig(userConfig, mockLoadConfigInit());
        expect(path.basename(config.srcIndexHtml)).toBe('index.html');
        expect(path.isAbsolute(config.srcIndexHtml)).toBe(true);
    });
    it('should set emptyDist to false', () => {
        userConfig.outputTargets = [
            {
                type: 'www',
                empty: false,
            },
        ];
        const { config } = validateConfig(userConfig, mockLoadConfigInit());
        expect(config.outputTargets[0].empty).toBe(false);
    });
    it('should set default emptyWWW to true', () => {
        const { config } = validateConfig(userConfig, mockLoadConfigInit());
        expect(config.outputTargets[0].empty).toBe(true);
    });
    it('should set emptyWWW to false', () => {
        userConfig.outputTargets = [
            {
                type: 'www',
                empty: false,
            },
        ];
        const { config } = validateConfig(userConfig, mockLoadConfigInit());
        expect(config.outputTargets[0].empty).toBe(false);
    });
    it('should set default collection dir and convert to absolute path', () => {
        userConfig.outputTargets = [
            {
                type: 'dist',
            },
        ];
        const { config } = validateConfig(userConfig, mockLoadConfigInit());
        expect(path.basename(config.outputTargets[0].collectionDir)).toBe('collection');
        expect(path.isAbsolute(config.outputTargets[0].collectionDir)).toBe(true);
    });
    it('should set default types dir and convert to absolute path', () => {
        userConfig.outputTargets = [
            {
                type: 'dist',
            },
        ];
        const { config } = validateConfig(userConfig, mockLoadConfigInit());
        expect(path.basename(config.outputTargets[0].typesDir)).toBe('types');
        expect(path.isAbsolute(config.outputTargets[0].typesDir)).toBe(true);
    });
    it('should set default build dir and convert to absolute path', () => {
        const { config } = validateConfig(userConfig, mockLoadConfigInit());
        const normalizedPathSep = path.sep;
        const parts = config.outputTargets[0].buildDir.split(normalizedPathSep);
        expect(parts[parts.length - 1]).toBe('build');
        expect(parts[parts.length - 2]).toBe('www');
        expect(path.isAbsolute(config.outputTargets[0].buildDir)).toBe(true);
    });
    it('should set build dir w/ custom www', () => {
        userConfig.outputTargets = [
            {
                type: 'www',
                dir: 'custom-www',
            },
        ];
        const { config } = validateConfig(userConfig, mockLoadConfigInit());
        const normalizedPathSep = path.sep;
        const parts = config.outputTargets[0].buildDir.split(normalizedPathSep);
        expect(parts[parts.length - 1]).toBe('build');
        expect(parts[parts.length - 2]).toBe('custom-www');
        expect(path.isAbsolute(config.outputTargets[0].buildDir)).toBe(true);
    });
    it('should set default src dir and convert to absolute path', () => {
        const { config } = validateConfig(userConfig, mockLoadConfigInit());
        expect(path.basename(config.srcDir)).toBe('src');
        expect(path.isAbsolute(config.srcDir)).toBe(true);
    });
    it('should set src dir and convert to absolute path', () => {
        userConfig.srcDir = 'app';
        const { config } = validateConfig(userConfig, mockLoadConfigInit());
        expect(path.basename(config.srcDir)).toBe('app');
        expect(path.isAbsolute(config.srcDir)).toBe(true);
    });
    it('should convert globalScript to absolute path, if a globalScript property was provided', () => {
        userConfig.globalScript = path.join('src', 'global', 'index.ts');
        const { config } = validateConfig(userConfig, mockLoadConfigInit());
        expect(path.basename(config.globalScript)).toBe('index.ts');
        expect(path.isAbsolute(config.globalScript)).toBe(true);
    });
    it('should convert globalStyle string to absolute path array, if a globalStyle property was provided', () => {
        userConfig.globalStyle = path.join('src', 'global', 'styles.css');
        const { config } = validateConfig(userConfig, mockLoadConfigInit());
        expect(path.basename(config.globalStyle)).toBe('styles.css');
        expect(path.isAbsolute(config.globalStyle)).toBe(true);
    });
});
//# sourceMappingURL=validate-paths.spec.js.map