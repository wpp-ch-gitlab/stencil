import type * as d from '../../../declarations';
export declare const createWebWorkerMainController: (sys: d.CompilerSystem, maxConcurrentWorkers: number) => d.WorkerMainController;
