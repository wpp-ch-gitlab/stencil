export declare function setupGlobal(gbl: any): any;
export declare function teardownGlobal(gbl: any): void;
export declare function patchWindow(winToBePatched: any): void;
export declare function addGlobalsToWindowPrototype(mockWinPrototype: any): void;
