import type * as d from '../../declarations';
export declare const validateTranspiledComponents: (config: d.Config, buildCtx: d.BuildCtx) => void;
